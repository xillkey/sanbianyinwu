package com.Admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.Public.JDBConnection;

/**
 * Servlet implementation class DealDocItem
 */
//@WebServlet("/DealDocItem")
public class DealDocItem extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DealDocItem() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
			String itemID = request.getParameter("itemID");
			response.setContentType("text/html;charset=UTF-8");
			PrintWriter out = response.getWriter();
			if(itemID == null)
			{
				out.print("出错啦");
			}else {
				JDBConnection con = new JDBConnection();
				ResultSet itemStatuts = con.executeQuery("select itemStatus from yx_docitem where itemID="+itemID);
				try {
					if(itemStatuts!=null&&itemStatuts.next())
					{
						if(itemStatuts.getInt(1)==3)
						{
							if(con.executeUpdate("update yx_docitem set itemStatus = 4 where itemID="+itemID)>0)
							{
								out.print("该订单状态已设为<font color='#64DA16'>交易完成</font>");
								
							}else {
								out.print("出错啦");
							}
						}else {
							out.print("你无权限进行该操作");
						}
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					out.print("出错啦");
					e.printStackTrace();
				}
				
				con.closeConnection();
			}
	}
	

}
