package com.User;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;



import com.Public.JDBConnection;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
/*
 *  接收订单 servlet
 *  code by xillkey
 * 
 */
public class ReceiveDocItem extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);

	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//add
		request.setCharacterEncoding("utf-8");
		
		Map<String, String> formData = new HashMap<String, String>();
		DiskFileItemFactory factory = new DiskFileItemFactory();
		factory.setRepository(new File(request.getRealPath("/Cache")));
		factory.setSizeThreshold(1024 * 1024 * 20);
		ServletFileUpload upload = new ServletFileUpload(factory);
		upload.setHeaderEncoding("UTF-8");
		List<FileItem> items = null;
		try {
			items = upload.parseRequest(request);
		} catch (FileUploadException e) {
			e.printStackTrace();
		}
		for (int i = 0; i < items.size(); i++) {
			FileItem item = (FileItem) items.get(i);
			if (item.isFormField()) // 非文件域
			{
				formData.put(item.getFieldName(), item.getString("UTF-8"));

			} else {
				String fileName = item.getName();
				System.out.println(fileName);
				SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmm");// 定义格式，不显示毫秒

				Timestamp now = new Timestamp(System.currentTimeMillis());// 获取系统当前时间
				String str = df.format(now);
				
				
				fileName = str + fileName.substring(fileName.lastIndexOf("."),
								fileName.length());
				
				formData.put("docFile", fileName);
				File filedis = new File(request.getRealPath("/Upload"));
				 if(!filedis.exists())
					 if(!filedis.mkdir())
						 System.out.println("创建Upload失败!");
				String path = request.getRealPath("/Upload") + "\\" + fileName;
				System.out.println(path);
				FileOutputStream fos = new FileOutputStream(path);
				InputStream is = item.getInputStream();
				byte[] buffer = new byte[1024];
				int len;
				while ((len = is.read(buffer)) > 0) {
					fos.write(buffer, 0, len);
				}
				is.close();
				fos.close();
			}
		}
		
		DocItem item=new DocItem();
		
		//add==================================
		//System.out.println("=====================================");
		//System.out.println("printType");
		//System.out.println(formData.get("printType"));
		item.setprintType(Integer.parseInt(formData.get("printType")));
		//System.out.println("paperType");
		//System.out.println(formData.get("paperType"));
		item.setpaperType(formData.get("paperType"));
		if(Integer.parseInt(formData.get("printType")) == 2){
			//System.out.println("ppts");
			//System.out.println(formData.get("ppts"));
			item.setppts(Integer.parseInt(formData.get("ppts")));
		}
		//System.out.println("copies");
		//System.out.println(formData.get("copies"));
		item.setcopies(Integer.parseInt(formData.get("copies")));
		//System.out.println("isSingle");
		//System.out.println(formData.get("isSingle"));
		item.setisSingle(Integer.parseInt(formData.get("isSingle")));
		//System.out.println("isColor");
		//System.out.println(formData.get("isColor"));
		item.setisColor(Integer.parseInt(formData.get("isColor")));
		//System.out.println("=====================================");
		//=====================================
		//System.out.println(formData.get("docFile"));
		item.setdocFile(formData.get("docFile"));
		//System.out.println(formData.get("paperType"));
		item.setotherRequest(formData.get("otherRequest"));
		item.setTrueName(formData.get("TrueName"));
		item.setstudentNum(formData.get("studentNum"));
		//System.out.println(formData.get("TrueName"));
		//System.out.println(formData.get("studentNum"));
		//System.out.println(formData.get("otherRequest"));
		item.setuserID(Integer.parseInt(formData.get("userID")));
		item.setitemType(Integer.parseInt(formData.get("itemType")));
		item.setitemStatus(1);
		item.setSubTime(System.currentTimeMillis());
		DBconnect.DocItemAddItem(item);
		if(formData.get("haveTrueName").equals("0"))// 说明在提交订单时才填真实姓名，需要更新user表。
		{
			String sql="update yx_user set TrueName='"+formData.get("TrueName")+"' ,studentNum='"+formData.get("studentNum")+"' where userID="+formData.get("userID");
			JDBConnection con=new JDBConnection();
			con.executeUpdate(sql);
			con.closeConnection();
		}
		

	}
}
