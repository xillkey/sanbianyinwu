package com.User;

import java.text.SimpleDateFormat;

public class DocItem {

	private int itemID;
	private int itemType;
	private int printType;
	private int userID;
	private int adminID;
	private String studentNum;
	private String TrueName;
	private String paperType;
	private int copies;
	private int isColor;
	private int isSingle;
	private int ppts;
	private String otherRequest;
	private int itemStatus;
	private String docFile;
	private long subTime;
	private long takeTime;
	private float money;
	public long getSubTime() {
		return subTime;
	}

	public void setSubTime(long subTime) {
		this.subTime = subTime;
	}

	public long getTakeTime() {
		return takeTime;
	}

	public void setTakeTime(long takeTime) {
		this.takeTime = takeTime;
	}


	public void setitemID(int itemID) {
		this.itemID = itemID;
	}

	public void setitemType(int itemType) {
		this.itemType = itemType;
	}

	public void setprintType(int printType) {
		this.printType = printType;
	}

	public void setuserID(int userID) {
		this.userID = userID;
	}

	public void setadminID(int adminID) {
		this.adminID = adminID;
	}

	public void setstudentNum(String stdno) {
		this.studentNum = stdno;
	}

	public void setTrueName(String TrueName) {
		this.TrueName = TrueName;
	}

	public void setpaperType(String paperType) {
		this.paperType = paperType;
	}

	public void setcopies(int copies) {
		this.copies = copies;
	}

	public void setisColor(int isColor) {
		this.isColor = isColor;
	}

	public void setisSingle(int isSingle) {
		this.isSingle = isSingle;
	}

	public void setppts(int ppts) {
		this.ppts = ppts;
	}

	public void setotherRequest(String otherRequest) {
		this.otherRequest = otherRequest;
	}

	public void setitemStatus(int itemStatus) {
		this.itemStatus = itemStatus;
	}

	public void setdocFile(String docFile) {
		this.docFile = docFile;
	}




	public void setmoney(float money) {
		this.money = money;
	}

	public int getitemID() {
		return this.itemID;
	}

	public int getitemType() {
		return this.itemType;
	}

	public int getprintType() {
		return this.printType;
	}

	public int getuserID() {
		return this.userID;
	}

	public int getadminID() {
		return this.adminID;
	}

	public String getstudentNum() {
		return this.studentNum;
	}

	public String getTrueName() {
		return this.TrueName;
	}

	public String getpaperType() {
		return this.paperType;
	}

	public int getcopies() {
		return this.copies;
	}

	public int getisColor() {
		return this.isColor;
	}

	public int getisSingle() {
		return this.isSingle;
	}

	public int getppts() {
		return this.ppts;
	}

	public String getotherRequest() {
		return this.otherRequest;
	}

	public int getitemStatus() {
		return this.itemStatus;
	}

	public String getdocFile() {
		return this.docFile;
	}

	public String getFormatsubTime() {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		java.sql.Timestamp subTime = new java.sql.Timestamp(this.subTime);
		String str = df.format(subTime);
		return str;
	}

	public String getFormattakeTime() {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		java.sql.Timestamp takeTime = new java.sql.Timestamp(this.takeTime);
		String str = df.format(takeTime);
		return str;
	}

	public float getmoney() {
		return this.money;
	}

}
