﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@ include file="../../WEB-INF/head.html" %>
<title>留言板</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
	
	textarea{
    height: 132px;
    width: 340px;
    resize: none;
    overflow: auto;
    color: white;
    border: none;
    background: none;
    margin-top: 95px;
    margin-left: 35px;

	}
	
	.message{
    width:470px;
    height: 310px;
    background-image: url(images/msg/msg.png);
    background-repeat: no-repeat;
    margin-left:auto;
    margin-right:auto;
    margin-top:30px;
	}

  #submit
  {
    background-image: url(images/msg/button.png);
    background-repeat: no-repeat;
    width:418px;
    height:52px;
    position: relative;
    border-radius: 10px;
    margin-top: 20px;
    border:none;
    background-color:none;
  }

</style>
</head>
<body bgcolor="#ffffff">
<%@ include file="../../WEB-INF/header.jsp" %>
<div class="message">

  <textarea id="message" onclick="this.innerHTML=''">点击留言</textarea>

  <button id="submit" class="button" onclick="submit()"></button>
</div>

	  <div id="alert" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="true">
	  	<div class="modal-header">
	    	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    	<h3 id="myModalLabel">提示</h3>
	  	</div>
			  <div class="modal-body" id="msg">
			  </div>
			  <div class="modal-footer">
			    <button class="btn" data-dismiss="modal" aria-hidden="true">关闭</button>
			  </div>
</div>

<script type="text/javascript">
	function submit()
	{
		
		 $.post("Message?action=submitMessage",{message:$("#message").val()},function(data)
					{
					  
					  $("#msg").html(data);
					  $("#alert").modal("show");
					  
					  
					  
					});
	}
</script>
</body>
</html>
