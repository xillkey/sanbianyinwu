<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>


<!DOCTYPE HTML>
<html lang="en">
<head>
<%@ include file="../../WEB-INF/head.html" %>
<%-- <base href="<%=basePath%>"> --%>
<title>三边校园</title>
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<link href="css/style.css" rel="stylesheet">
<style type="text/css">
body {
	padding-top: 60px;
	padding-bottom:0
}

</style>
</head>

<body>
<%@ include file="../../WEB-INF/header.jsp" %>
<!-- 	<button onclick="window.location.href='login.jsp'">登陆</button><br>
	<button onclick="window.location.href='user/register/register.jsp'">注册</button> -->
	<div class="container" >

      <!-- Main hero unit for a primary marketing message or call to action -->
      <div id="myCarousel" class="carousel slide" style=" width: 960px; margin:0 auto;">
                <ol class="carousel-indicators">
                  <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                  <li data-target="#myCarousel" data-slide-to="1"></li>
                  <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                  <div class="item active">
                    <img src="images/1.png" alt="">
                    <div class="carousel-caption">
                    </div>
                  </div>

                </div>
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a>
              </div>

      <!-- Example row of columns -->
      <div class="row" style="width:960px; margin:20px auto;">
        <div class="span3" style="width:319px;border-right:1px dashed #E4E4E4 ">
          <div class="print"><img src="images/print.png" alt="文档打印"/></div>
          <h2>文档打印下单</h2><div align="center">仅限五山宿舍</div>
          <p style="text-align:center"><a class="btn btn-success"  href="WordBlack.jsp">我要下单 &raquo;</a></p>
        </div>
        <div class="span3" style="width:319px;border-right:1px dashed #E4E4E4">
		  <div class="card"><img src="images/card.png" alt="名片印刷"/></div>
          <h2>名片印刷</h2>
          <div align="center">仅限五山宿舍</div>
          <p style="text-align:center"><a class="btn" href="#">敬请期待 &raquo;</a></p>
        </div>
        <div class="span3" style="width:319px">
		  <div class="book"><img src="images/book.png" alt="教材打印"/></div>
          <h2>教材打印</h2>
          <div align="center">仅限五山宿舍</div>
          <p style="text-align:center"><a class="btn" href="#">敬请期待 &raquo;</a></p>
        </div>
      </div>
     </div>
<footer>
   <div class="ftContent">
   		<p><a href="adminlogin.jsp">[管理入口]</a></p>
      <p>Copyright © 2013. All rights reserved <span>三边校园倾情打造</span></p>
   </div>
</footer>
</body>
</html>
