﻿<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>内部员工订单登录系统</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="images/favicon.ico">
<!-- Le styles -->
<link href="http://cdnjs.bootcss.com/ajax/libs/twitter-bootstrap/2.3.1/css/bootstrap.min.css" rel="stylesheet">
<style type="text/css">
body {
	padding-top: 30px;
	background-color: #EBE8DD;
	font-family: Microsoft YaHei;
}
</style>
<link href="css/style.css" rel="stylesheet">
<link href="http://cdnjs.bootcss.com/ajax/libs/twitter-bootstrap/2.3.1/css/bootstrap-responsive.min.css" rel="stylesheet">

<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="//cdnjs.bootcss.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
<![endif]-->

<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="../assets/ico/favicon.png">
</head>

<body>
<div class="wrapper"></div>
<div class="container">
<form class="form-signin" action="LoginServlet" method="POST">
<h5 class="form-signin-heading">后台登陆</h5>
<input type="hidden" name="usertype" value="admin">

<input type="text" class="input-block-level" placeholder="用户名" name="userName">
<input type="password" class="input-block-level" placeholder="密码" name="password">
<button class="btn btn-large btn-primary" type="submit">登陆</button>
</form>
</div>
<div class="footer">
  <p>Copyright © 2013.All rights reserved.</p>
  <p>@三边网络技术中心倾情奉献</p>
</div>
<!-- /container --> 
<script src="http://cdnjs.bootcss.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> 
<script src="http://cdnjs.bootcss.com/ajax/libs/twitter-bootstrap/2.3.1/js/bootstrap.min.js"></script>
</body>
</html>