<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<html>
<head>
	<title>个人文档类打印下单</title>
	<style type="text/css">
		*{
			font-family:微软雅黑;
		}
		input.b2{
		background-color: #CC0000;
		 border:1 solid #DEE3E7; 
		 COLOR: #ffffff; 
		 FONT-SIZE: 9pt; 
		 FONT-STYLE: normal; 
		 FONT-VARIANT: normal; 
		 FONT-WEIGHT: normal; 
		 HEIGHT: 30px; 
		 LINE-HEIGHT: normal}
		#TrueName
		{
			width:350px;
			height:150px;
			padding:50px;
			position:absolute;
			top:30%;
			left:30%;
			background-color:#F0EEC5;
			display:none;
			border-radius:5px;
			
		}
		#TrueName-nav1
		{
			height:22px;
			line-height:22px;
			margin-bottom:20px;
			
		}
		.TrueName-nav2
		{
			float:left;
			text-align:right;
			width:75px;
			margin-left:10px;
			margin-right:10px;
		}
		.TrueName-nav3
		{
			float:left;
		}
		.error
		{
			color:red;
			font-size:12px;

		}
		ul.red{
	padding: 5px;
	margin: 10px 0;
	list-style: none;
	float: left;
	clear: left;
	margin-left: 250px;
	margin-top: 40px;
	}

	ul.red li {
		float: left;
		}
	
	ul.red li a {
		float: left;
		text-decoration: none;
		color: #ccc;
		padding: 4px 15px 0 0;
		margin-right: 8px;
		font: 900 14px "Arial", Helvetica, sans-serif;
		}
	
	ul.red li a span {
		float: left;
		padding-right: 15px;
		display: block;
		margin-top: -4px;
		height: 24px;
	}
	
	ul.red li a:hover {
	 	color: #fff;
		background: url(images/red.png) no-repeat top right;
	}
	
	ul.red li a:hover span {
		background: url(images/red.png) no-repeat top left;
	}
	
	ul.red li a.current {
		background: url(images/red.png) no-repeat top right;
	 	color: #fff;
	}
	
	ul.red li a.current span {
		background: url(images/red.png) no-repeat top left;
	}
	</style>
</head>
<meta  charset="utf-8">
<body>
	<form method="post" action="ReceiveDocItem" enctype="multipart/form-data" id="itemform">
	<input type="hidden" name="userID" id="userID">
	<input type="hidden" name="itemType" value="1">
	<table style="width:500px;">
	<tr>
		<td colspan="2" align="center"><h2>个人文档类打印下单</h2></td>
	</tr>
			<tr>
				<td align="right" style="width:202px;">文档类型</td><td align="left">
				<select name="printType" id="printType" onchange="selectDocType(this.value)">
					<option value="1">doc/docx</option>
					<option value="2">ppt/pptx</option>
					<option value="3">xls/xlsx</option>
				</select>
				</td>
			</tr>
			<tr>
				<td align="right">纸张大小</td><td align="left"><select name="paperType" id="paperType">
					<option value="A4">A4</option>
					<option value="A3">A3</option>
					<option value="B4">B4</option>
					<option value="B5">B5</option>
				</select>
				</td>
			</tr>
			<tr><td colspan="2"><div  id="ppt" style="display:none">
				每张纸打印ppt幻灯片个数：
				
				<select name="ppts" id="ppts">
					<option value="1">1</option>
					<option value="2">2</option> 
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="6">6</option>
					<option value="9">9</option>
				</select>
				</div>
				</td>
			</tr>
			<tr>
				<td align="right">打印份数：</td><td align="left"><input type="text" name="copies" id="copies"></td>
			</tr>
			<tr>
				<td align="right">单/双面：</td><td align="left"><input type="radio" name="isSingle" value="1" checked="checked">单面<input type="radio" name="isSingle" value="0">双面 	</td>	
			</tr>
			<tr>
				<td align="right">是否彩色：</td><td align="left"><input type="radio" name="isColor" value="1">彩色<input type="radio" name="isColor" value="0" checked="checked">黑白 </td>
			</tr>
			<tr>
				<td align="right">文件上传：</td><td align="left"><input type="file" name="docFile" id="upload" /></td>
			</tr>
			<tr>
				<td align="right">其他要求：</td><td align="left"><textarea cols="30" rows="5" name="otherRequest"></textarea></td> 
			</tr>
			
			
			<tr>
				<td colspan="2" align="center"><input type="button" value="提交订单" onclick="checkall()" class="b2"> </td>
			</tr>
		
	</table>
	<div id="TrueName">
		<div id="TrueName-nav1">填写真实信息，以便取货时核对身份</div>
		<table>
		<tr>
			<td><div class="TrueName-nav2">真实姓名:</div></td><td><div class="TrueName-nav3"><input type="text" name="TrueName" id="NameInput" onblur="checkTrueName()" ></div></td><td><div class="error" id="NameInputErr"></div></td>
		</tr>
		<tr>
			<td><div class="TrueName-nav2">联系电话:</div></td><td><div class="TrueName-nav3"><input type="text" name="phone" id="phone" onblur="checkphone()"></div></td><td><div class="error" id="phoneErr"></div></td>
		</tr>
		<tr>
					<td><input type="hidden" name="haveTrueName" value="1" id="haveTrueName"></td>
		</tr>
		</table>
		<ul class="red">
			<li><a href="#"  class="current" onclick="checkTwo()"><span></span>下一步</a></li>
		</ul>
	</div>
	</form>
	<script type="text/javascript">
	if(getCookie("LoginID")==""||getCookie("LoginID")=="\"\""||getCookie("LoginID")==null)
		alert("登陆超时，请重新登录");
	function isnum(num){
		var re=/[^0-9]/;
		return re.test(num);
	}
	function isEmpty(str)
	{
		 for(var i=0;i<str.length;i++)
		 {
		  if(str.charAt(i)!==" ")
		  {
		    return false;
		  }
		 }
		 return true;
	}
	 function getCookie(name) {
		 if(!String.prototype.trim) {
			    String.prototype.trim = function() { return this.replace(/(^s*)|(s*$)/g, ""); }
			}
		   var cookies = document.cookie.split(";");
		   for(var i=0;i<cookies.length;i++) {
		    var cookie = cookies[i];
		    var cookieStr = cookie.split("=");
		    if(cookieStr && cookieStr[0].trim()==name) {
		     return  decodeURI(cookieStr[1]);
		    }
		   }
		 }
		 

	function selectDocType(type)
	{ 
		document.getElementById("ppt").style.display="none";
				if(type==2)
				{
				document.getElementById("ppt").style.display="block";
				document.getElementById("ppts").name="ppts";
				}
		
	}

function checkfile(){
 var fileName = document.getElementById('upload').value;
 var  copies=document.getElementById("copies").value;
 if(isEmpty(copies))
	 {
	 	alert("请输入份数");
	 	return false;
	 }
 if(isEmpty(fileName)){
    alert("请选择文件");
    document.getElementById('upload').focus();
    return false;
 }
 //lastIndexOf如果没有搜索到则返回为-1
    if(fileName.lastIndexOf(".")!=-1)
 	{
    var printType=parseInt(document.getElementById('printType').value);
    var fileType = (fileName.substring(fileName.lastIndexOf(".")+1,fileName.length)).toLowerCase();
       var suppotFile = new Array();
  	switch(printType)
  	{
  		case 1:suppotFile[0] = "doc";suppotFile[1] = "docx";break;
  		case 2:suppotFile[0] = "ppt";suppotFile[1] = "pptx";break;
  		case 3:suppotFile[0] = "xls";suppotFile[1] = "xlsx";break;
  	}
    for(var i =0;i<suppotFile.length;i++){
       if(suppotFile[i]==fileType){
     return true;
    }else{
   			continue;
   		 }
 	 }
  	alert("不支持文件类型"+fileType+"，请正确选择文档类型");
  	return false;
	 }else
	 {
	  alert("文件只支持主流文档格式");
	 }
}
function checkTrueName()
{
	document.getElementById("NameInputErr").innerHTML = "";
	if(isEmpty(document.getElementById("NameInput").value))
		{	
			document.getElementById("NameInputErr").innerHTML = "姓名不能为空！";
			return false;
		}
	
	return true;
}

function checkphone()
{
	document.getElementById("phoneErr").innerHTML = "";
	ssn=document.getElementById("phone").value;
	if(ssn.length == 0){
		document.getElementById("phoneErr").innerHTML = "号码不能为空！";
		return false;
	}
	else if(isnum(ssn)){
		document.getElementById("phoneErr").innerHTML = "<font color='red'>应该全为数字</font>";
		return false;
	}
	document.getElementById("phoneErr").innerHTML = "";
	return true;
	
}

function checkall()
{
	document.getElementById("userID").value=getCookie("LoginID");
	if(!checkfile()) return;
	if (getCookie("phone")==""||getCookie("phone")=="\"\""||getCookie("phone")==null||getCookie("TrueName")=="\"\""||getCookie("TrueName")==""||getCookie("TrueName")==null)
	{
		document.getElementById("TrueName").style.display="block";
		document.getElementById("haveTrueName").value="0";
		
	}else
	{
		document.getElementById("NameInput").value=getCookie("TrueName");
		document.getElementById("phone").value=getCookie("phone");
		document.getElementById("itemform").submit();
	}
}

function checkTwo()
{
	if(checkTrueName()&&checkphone())
	{
		document.getElementById("itemform").submit();
	}	
}

</script>
</body>
</html>