<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="com.sanbianyinwu.www.Public.DocItem" %>    

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>查看订单详情</title>
</head>
<body>
    <%
    	DocItem DI=(DocItem)request.getAttribute("item");
        if(DI!=null)
        {
    %>

    <table class="table table-bordered" style="width:500px;">
		<tr>
			<td>订单类型</td>
			<td>
			<%
			switch(DI.getitemType())
			{
			case 1: out.print("个人");break;
			case 2: out.print("班级");break;
			case 3: out.print("社团");break;
			}
			%>
			</td>
			<td>打印类型</td>
			<td>
			 <%
				switch(DI.getprintType())
				{
				case 1: out.print("doc/docx");break;
				case 2: out.print("ppt/pptx");break;
				case 3: out.print("xls/xlsx");break;
				}
			 %>
			</td>
		</tr>
		<tr>
			<td>联系方式</td>
			<td>
				<%=DI.getstudentNum() %>
			</td>
			<td>姓名</td>
			<td>
				<%=DI.getTrueName() %>
			</td>
		</tr>
		<tr>
			<th>订单号</th><th><%=DI.getdocFile().substring(0, DI.getdocFile().lastIndexOf('.')) %></th>
			<td>订单状态</td>
			<td>
			<%
				switch(DI.getitemStatus())
				{
				 case 1:out.print("等待处理");break;
				 case 2:out.print("订单审核");break;
				 case 3:out.print("处理完成");break;
				 case 4:out.print("交易完成");break;
				 case 5:out.print("订单出错");break;
				 case 6:out.print("用户取消");break;
				}
			%>
			</td>
		</tr>
		<tr>
			<td>打印参数</td>
			<td colspan="3" style="height:80px;">
			纸张类型:<%=DI.getpaperType() %><br>
			打印份数:<%=DI.getcopies() %><br>
			<%
				switch(DI.getisColor())
				{
				case 1:out.print("彩色");break;
				case 0:out.print("黑白");break;
				}
			%><br>
			<%
					switch(DI.getisSingle())
					{
					case 1:out.print("单面");break;
					case 0:out.print("双面");break;
					}
				%><br>
				<%=DI.getppts()==0?"":"每页幻灯片张数:"+DI.getppts()+"<br>" %>
			<%=DI.getotherRequest()==null?"无":DI.getotherRequest() %>
			</td>
	
		</tr>
		<tr>
			<td>提交日期</td>
			<td>
			<%=DI.getFormatsubTime() %>
			</td>
			<td>成交金额</td>
			<td>
			<%=DI.getmoney()==0?"未知":DI.getmoney() %>
			</td>
		</tr>		
		<tr>
			<td>出货方式</td><td>自取</td>
			<td>出货时间</td><td>隔天</td>
		</tr>					
	</table>

	<%}else {%>不存在此订单<%} %>
</body>

</html>