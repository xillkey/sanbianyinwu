<%@page import="com.sanbianyinwu.www.Public.Message.Message_Info"%>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="java.util.List"%>

<%
	
	List<Message_Info> msList =(List<Message_Info>)request.getAttribute("msList");
	String type = (String)request.getAttribute("type");
	Integer totalpage = 1;
	totalpage = (Integer) request.getAttribute("totalpage");
	int curpage = request.getParameter("page")==null?1:Integer.parseInt(request.getParameter("page"));
	
%>

<html>
<head>
	<%@ include file="../../WEB-INF/head.html" %>
	<meta charset="UTF-8">
	<title>留言</title>
</head>
<body>
 <%@ include file="../../WEB-INF/header.jsp" %>
  <div class="container">
		<h2>留言</h2>
<%if("admin".equals(type)){ %>
<div style="float:right;" class="input-append">
<form action="Message" method="get" >
	<select style="float:left" name="action">
		<option value="allMessages">请选择用户类型</option>
		<option value="allMessages">所有人留言</option>
  		<option value="adminMessages">管理员留言</option>
  		<option value="userMessages">用户留言</option>
	</select>
	<select style="float:left" name="show">
		<option value="all">请选择留言状态</option>
  		<option value="all">所有留言</option>
  		<option value="haveread">已读留言</option>
  		<option value="havenotread">未读留言</option>
  		<option value="marked">点赞留言</option>
	</select>
	<input type="submit" value="筛选" class="btn">
</form>
</div>
<%} %>
		<table class="table table-hover" style="width:1000px;text-align:center;margin:0 auto;">
			<thead>
				<tr>
					<th>留言时间</th>
					<th>留言者</th>
					<th>留言者类型</th>
					<%if("admin".equals(type)){ %>
					<th>状态</th>
					<%} %>
					<th>意见点赞</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
			<%if(msList==null||msList.size()<1){ %>
			<tr><td colspan="6">没有数据</td></tr>
			<%}else { for(Message_Info ms : msList){%>
				<tr>
					<td><%=ms.getFormatsubTime() %></td>
					<td><% if(ms.getAdminID()==0){
						out.print(ms.getUserName());
						}
					else if(ms.getUserID()==0){
						out.print(ms.getAdminName());
					}
					%></td>
					<td><% if(ms.getAdminID()==0){
						out.print("用户");
						}
					else if(ms.getUserID()==0){
						out.print("管理员");
					}
					%></td>
					<%if("admin".equals(type)){ %>
					<td><% if(ms.getHave_read()==0)out.print("未读");else out.print("已读"); %></td>
					<%} %>
					<td><%if(ms.getMark()==0)out.print("无");else out.print("点赞"); %></td>
					<td><button msgID="<%=ms.getMessageID()%>" class="btn btn-primary viewMsg">查看留言</button></td>
				</tr>
				<%} %>
			<%} %>	
			</tbody>
		</table>
		
		<ul class="pager">
		<%if(curpage!=1) {%>
		  <li><a href="Message<%=(String)request.getAttribute("query")+"&page="+(curpage-1) %>">上一页</a></li>
		  <%} %>
		  <li>第 <%=curpage %> 页 共 <%=totalpage %>页 </li>
		 <%if(curpage!=totalpage.intValue()){ %> 
		  <li><a href="Message<%=(String)request.getAttribute("query")+"&page="+(curpage+1) %>">下一页</a></li>
		  <%} %>
		</ul>
	</div>	
		<div id="msg" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="true">		
			<div class="modal-header">
	   			 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    		 <h3 id="myModalLabel">留言</h3>
	  		</div>
	  		<div class="modal-body" id="msgDetail">
	  		</div>
	  		<div class="modal-footer">
	  		<%if("admin".equals(type)){ %>
	  			<input type="hidden" id="msgID">	  			
	  			<button class="btn btn-primary mark" >意见点赞</button>
	  		<%} %>
	    		<button class="btn" data-dismiss="modal" aria-hidden="true">关闭</button>
	  		</div>
	  </div>
	  <div id="alert" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="true">
	  	<div class="modal-header">
	    	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    	<h3 id="myModalLabel">提示</h3>
	  	</div>
			  <div class="modal-body" id="data">
			  </div>
			  <div class="modal-footer">
			    <button class="btn" data-dismiss="modal" aria-hidden="true">关闭</button>
			  </div>
	  </div>	
<script type="text/javascript">
	$(".viewMsg").click(
		function()
		{
			var messageID = $(this).attr("msgID");
			$("#msgID").val(messageID);
			 $.get("Message?action=viewMessage",{messageID:messageID},function(data)
			{
				$("#msgDetail").html(data);
				$("#msg").modal("show");
			  
			});
		}
	);
	$(".mark").click(function()
	{
		var messageID = $("#msgID").val();
		 $.get("Message?action=markMessage",{messageID:messageID},function(data)
					{
						$("#data").html(data);
						$("#msg").modal("hide");
						$("#msg").on('hidden',
						function()
						{
							$("#alert").modal("show");
							$("#alert").on('hidden',function()
							{
								if(data=="点赞成功")
								location.reload();
							}		
							);
							
						}
						);
					  
					});
	}		
	);

</script>	  	
</body>
</html>