﻿    <%@page import="com.sanbianyinwu.www.Public.Function_Library"%>
    <%@page import="java.net.URLDecoder"%>
<%
    	Cookie[] cookies = request.getCookies();
    	Cookie loginNameCookie = Function_Library.findCookie(cookies, "LoginName");
    	Cookie typeCookie = Function_Library.findCookie(cookies, "type");
    	String name = null;
    	if(loginNameCookie!=null&&!loginNameCookie.getValue().equals(""))
    	{
    		name = loginNameCookie.getValue();
    	}
    
    %>
    
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="brand" href="#"><img src="images/logo.png" class="blogo"/>三边校园</a>
          <div class="nav-collapse collapse">
            <ul class="nav">
              <li><a href="index.jsp">主页</a></li>
              <li><a href="#about">业务介绍</a></li>
              <li><a href="message.jsp">意见箱</a></li>
            </ul>
          </div>
          <%if(name==null) {%>
            <form name="form" class="navbar-form pull-right" id="SignIn" action="LoginServlet" method="POST">
              <input class="span2" type="text" placeholder="用户名,邮箱或学号" name="userName">
              <input class="span2" type="password" placeholder="密码" name="password">
				<input type="hidden" name="logintype" value="name">
              <input type="hidden" name="usertype" value="user">
              <input type="submit" class="btn" onclick="setLoginStyle()" value="登陆">
              <input type="button"  class="btn btn-primary" onclick="window.location.href='UserRegister'" value="注册">
            </form>
            <% }else{
            if("user".equals(typeCookie.getValue()))
            {
            %>
            
            <ul class="nav pull-right" id="UserInf">
                    <li class="dropdown">
                      <a class="dropdown-toggle" id="user-welcome" ><%="你好,"+name %></a>
                    </li>
                    <li id="fat-menu" class="dropdown">
                      <a href="#" id="drop3" role="button" class="dropdown-toggle" data-toggle="dropdown">我的订单<b class="caret"></b></a>
                      <ul class="dropdown-menu" role="menu" aria-labelledby="drop2">
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="MyDocItem">我的文档订单</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="Message?action=myMessages">查看我的留言</a></li>
                   </ul>
                  </li>
                    <li id="fat-menu" class="dropdown">
                      <a href="#" id="drop3" role="button" class="dropdown-toggle" data-toggle="dropdown">账号<b class="caret"></b></a>
                      <ul class="dropdown-menu" role="menu" aria-labelledby="drop3">
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="EditUserInf">账号设置</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="logout.jsp">退出登录</a></li>
                   </ul>
                  </li>
            </ul>
            <%}else if("admin".equals(typeCookie.getValue())){ 
            Cookie adminRightCookie = Function_Library.findCookie(cookies, "adminRight");
            String adminRight = URLDecoder.decode(adminRightCookie.getValue(), "UTF-8");
            %>
             <ul class="nav pull-right" id="UserInf">
                    <li class="dropdown">
                      <a class="dropdown-toggle"  ><%="你好,"+name %>[<%=adminRight %>]</a>
                    </li>
                    <li id="fat-menu" class="dropdown">
                  	<a href="#" id="drop3" role="button" class="dropdown-toggle" data-toggle="dropdown">我的<b class="caret"></b></a>
                      <ul class="dropdown-menu" role="menu" aria-labelledby="drop2">
						<li role="presentation"><a role="menuitem" tabindex="-1" href="AllDocItem">查询所有订单</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="AdminGetDocItem">我接手的订单</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="Message?action=allMessages">查看留言</a></li>
                   </ul>
                  </li>
                  <% if("超级管理员".equals(adminRight)){ %>
                  <li id="fat-menu" class="dropdown">
                      <a href="#" id="drop3" role="button" class="dropdown-toggle" data-toggle="dropdown">系统设置<b class="caret"></b></a>
                      <ul class="dropdown-menu" role="menu" aria-labelledby="drop3">
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="AddAdmin">添加管理员</a></li>
                   </ul>
                  </li>
                   <%} %>
                    <li id="fat-menu" class="dropdown">
                      <a href="#" id="drop3" role="button" class="dropdown-toggle" data-toggle="dropdown">账号<b class="caret"></b></a>
                      <ul class="dropdown-menu" role="menu" aria-labelledby="drop3">
                        
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="logout.jsp">退出登录</a></li>
                   </ul>
                  </li>
            </ul>
            <%} %>
		<%} %>
          </div><!--/.nav-collapse -->
        </div>
      </div>
     <script type="text/javascript" src="js/register.js"></script>
    <script type="text/javascript">
     function setLoginStyle(){
     	var s = document.form.userName.value;
     	var filter  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
     	var filterNum = /^\d+$/;
     	if (filter.test(s)){
     		document.form.logintype.value="email";
     	}else if(filterNum.test(s)){
     		document.form.logintype.value="number";
     	}else{
     		document.form.logintype.value="name";
     	}
     }
     function getCookie(c_name)
     {
     if (document.cookie.length>0)
       {
       c_start=document.cookie.indexOf(c_name + "=")
       if (c_start!=-1)
         { 
         c_start=c_start + c_name.length+1 
         c_end=document.cookie.indexOf(";",c_start)
         if (c_end==-1) c_end=document.cookie.length
         return decodeURI(document.cookie.substring(c_start,c_end));
         } 
       }
     return ""
     }

     </script> 