<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="java.util.List"%>
<%@page import="com.sanbianyinwu.www.Public.DocItem"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>所要查询的订单</title>

	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<script src="//cdnjs.bootcss.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
      <script src="//cdnjs.bootcss.com/ajax/libs/twitter-bootstrap/2.3.1/js/bootstrap.min.js"></script>
    <link href="//cdnjs.bootcss.com/ajax/libs/twitter-bootstrap/2.3.1/css/bootstrap.min.css" rel="stylesheet">
  </head>
    <script type="text/javascript">
  $(document).ready(function(){
	  	$(".viewItem").click(
	  		  	function()
	  		  	{
	  		  		var itemID = $(this).attr("itemID");
	  		  		$.get("ViewDocItem", {itemID: itemID},
	  		  			   function(data){
	  		  			    $("#itemDetail").html(data);
	  		  			    $("#item").modal('show');
	  		  			   } 
	  		  			); 
	  		  	}
	  		  	
	  		  	);
	  
  });

  </script>
  <body >
   <%@ include file="../../WEB-INF/header.jsp" %>
   <div class="container">
  <table class="table table-hover" style="width:1000px;text-align:center;margin:0 auto;">
	<tr>
				<th>
					订单类型
				</th>
				<th>
					提交者
				</th>
				<th>
					打印份数
				</th>
				<th>
					提交时间
				</th>
				<th>
					交易金额
				</th>
				<th>
					订单状态
				</th>
				<th>
					订单文件
				</th>
				<th>
					操作
				</th>
			</tr>
			
			
			<%
				// 获取图书信息集合
				List<DocItem> list = (List<DocItem>) request.getAttribute("list");
				// 判断集合是否有效
				if (list == null || list.size() < 1) {
					out.print("没有数据！");
				} else {
					// 遍历图书集合中的数据
					for (DocItem DI : list) {
			%>
			<tr align="center" bgcolor="white">
				<%
					String itemType;
					switch(DI.getitemType()){
					case(1):
						itemType = "个人";
						break;
					case(2):
						itemType = "班级";
						break;
					case(3):
						itemType = "社团";
						break;	
					default:
						itemType = "错误";
						break;
					}
					
					String itemStatus;
					switch(DI.getitemStatus()){
					case(1):
						itemStatus = "等待处理";
						break;
					case(2):
						itemStatus = "订单审核";
						break;
					case(3):
						itemStatus = "处理完成";
						break;
					case(4):
						itemStatus = "交易完成";
						break;
					case(5):
						itemStatus = "订单出错";
						break;
					case(6):
						itemStatus = "用户取消";
						break;
					default:
						itemStatus = "错误";
						break;
					}
				 %>
				<td><%=itemType%></td>
				<td><%=DI.getTrueName() %></td>
				<td><%=DI.getcopies()%></td>
				<td><%=DI.getFormatsubTime()%></td>
				<td><%=DI.getmoney()%></td>
				<td><%=itemStatus%></td>
				<td><%=DI.getdocFile() %></td>
				<td>
				<p><button itemID="<%=DI.getitemID() %>" class="btn btn-primary viewItem">查看详情</button>
				
				<button class="btn btn-success">处理订单</button>
				
				</p>
				</td>
			</tr>
			<%
					}
				}
			%>
			
		</table>
		
		
		<div align="center">
		<%
			int t, i;
			int n = Integer.parseInt(request.getParameter("page")==null?"1":request.getParameter("page"));
			int count =(Integer) request.getAttribute("count");
			if(count % 5 > 0)
				t = count / 5 + 1;
			else t = count / 5;
			int showpage = 10;
			%><%if(n != 1)
		{%>
		<a href="AdminGetDocItem?userID=<%=request.getParameter("userID")%>&page=<%=n-1%>">上一页</a>
		<%} 
			if(n <= (showpage + 1) / 2 || t <= showpage){
            	for(i = 1; i <= showpage && i <= t; i++){
            		if(i == n){%>
                	<%=i%>
                	<%
                	}
            		else{
            			%>
            			<a href="AdminGetDocItem?userID=<%=request.getParameter("userID")%>&page=<%=i%>" ><%=i%></a> <%
            			}
            		}
        	}else{
            	%>
            	<a href="AdminGetDocItem?userID=<%=request.getParameter("userID")%>&page=1" >1...</a> <%
            	for( i = n - (showpage - 1) / 2 < t - (showpage - 1) ? n - (showpage - 1) / 2 : t - (showpage - 1); i <= n + showpage / 2 && i <= t; i++){
                if(i == n){%>
                	<%=i%>
                	<%
                	}
            		else{%>
                <a href="AdminGetDocItem?userID=<%=request.getParameter("userID")%>&page=<%=i%>" ><%=i%></a> <%
                	}
                }
        	}
        	if(t - n >= showpage / 2 + 1 && t > showpage){
            	%>
            	<a href="AdminGetDocItem?userID=<%=request.getParameter("userID")%>&page=<%=t %>" >...<%=t %></a> <%
        	}%>
		<%if(n != t)
		{%>
		<a href="AdminGetDocItem?userID=<%=request.getParameter("userID")%>&page=<%=n+1%>">下一页</a>
		<%} %>
		<br>
		第<%=n%>页	共<%=t%>页
		</div>
		
	</div>
	<div id="item" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="true">		
		<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">订单详情</h3>
  </div>
  <div class="modal-body" id="itemDetail">
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">关闭</button>
  </div>
  
  </body>
</html>
