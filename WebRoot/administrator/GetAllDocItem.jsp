<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="java.util.List"%>
<%@page import="com.sanbianyinwu.www.Public.DocItem"%>
<!DOCTYPE html>
<html lang="en">
<html>
  <head>
    <title>所有订单</title>
<%@ include file="../../WEB-INF/head.html" %>
  </head>
  <script type="text/javascript">

  
  $(document).ready(function(){
	  	$(".viewItem").click(
	  		  	function()
	  		  	{
	  		  		var itemID = $(this).attr("itemID");
	  		  		$.get("ViewDocItem", {itemID: itemID},
	  		  			   function(data){
	  		  			    $("#itemDetail").html(data);
	  		  			    $("#item").modal('show');
	  		  			   } 
	  		  			); 
	  		  	}
	  		  	
	  		  	);
	  	$(".handleItem").click(
	  		function()
	  		{
	  			var itemID = $(this).attr("itemID");
  		  		$.get("HandleDocItem", {itemID: itemID},
	  		  			   function(data){
	  		  			    $("#msg").html(data);
	  		  			    $("#alert").modal('show');
	  		  			   } 
	  		  			); 
	  		}
	  	
	  	);
	  	
	  	$(".dealItem").click(
		  		function()
		  		{
		  			var itemID = $(this).attr("itemID");
	  		  		$.get("DealDocItem", {itemID: itemID},
		  		  			   function(data){
		  		  			    $("#msg").html(data);
		  		  			    $("#alert").modal('show');
		  		  			   } 
		  		  			); 
		  		}
		  	
		  	);
	  	
	    $("#alert").on("hidden",function()
	    		{
	    		  window.location.reload();
	    		}	  
	    	  );
	  
  });
  
  

  </script>
  <body >
  <div class="container">	
 <%@ include file="../../WEB-INF/header.jsp" %>
<h2>所有订单</h2>
<!-- 	<div class="input-append" style="float:right" >
	  <input class="span2" id="appendedInputButton" type="text" placeholder="输入订单号">
	  <button class="btn btn-primary" type="button">完成交易</button>
	</div> -->
  <table class="table table-hover" style="width:1000px;text-align:center;margin:0 auto;">
	<tr>
				<th>
					订单类型
				</th>
				<th>
					提交者
				</th>
				<th>
					打印份数
				</th>
				<th>
					提交时间
				</th>
				<th>
					交易金额
				</th>
				<th>
					订单状态
				</th>
				<th>
					订单文件
				</th>
				<th>
					操作
				</th>
			</tr>
			
			
			<%
				// 获取图书信息集合
				List<DocItem> list = (List<DocItem>) request.getAttribute("list");
				// 判断集合是否有效
				if (list == null || list.size() < 1) {
					out.print("没有数据！");
				} else {
					// 遍历图书集合中的数据
					for (DocItem DI : list) {
			%>
			<tr align="center" bgcolor="white">
				<%
					String itemType;
					switch(DI.getitemType()){
					case(1):
						itemType = "个人";
						break;
					case(2):
						itemType = "班级";
						break;
					case(3):
						itemType = "社团";
						break;	
					default:
						itemType = "错误";
						break;
					}
					
					String itemStatus;
					switch(DI.getitemStatus()){
					case(1):
						itemStatus = "等待处理";
						break;
					case(2):
						itemStatus = "订单审核";
						break;
					case(3):
						itemStatus = "处理完成";
						break;
					case(4):
						itemStatus = "交易完成";
						break;
					case(5):
						itemStatus = "订单出错";
						break;
					case(6):
						itemStatus = "用户取消";
						break;
					default:
						itemStatus = "错误";
						break;
					}
				 %>
				<td><%=itemType%></td>
				<td><%=DI.getTrueName() %></td>
				<td><%=DI.getcopies()%></td>
				<td><%=DI.getFormatsubTime()%></td>
				<td><%=DI.getmoney()%></td>
				<td><%=itemStatus%></td>
				<td><%=DI.getdocFile() %></td>
				<td>
				<p><button itemID="<%=DI.getitemID() %>" class="btn btn-primary viewItem">查看详情</button>
				<%if(DI.getitemStatus() == 1){ %>
				<button class="btn btn-success handleItem" itemID="<%=DI.getitemID() %>">处理订单</button>
				<%} %>
				<%if(DI.getitemStatus() == 3){ %>
				<button class="btn btn-success dealItem" itemID="<%=DI.getitemID() %>">完成交易</button>
				<%} %>
				</p>
				</td>
			</tr>
			<%
					}
				}
			%>
			
		</table>
		
		
		
		<div class="pagination" style="margin:0 auto;" align="center">
			<ul>
		<%
			int t, i;
			int n = Integer.parseInt(request.getParameter("page")==null?"1":request.getParameter("page"));
			int count =(Integer) request.getAttribute("count");
			if(count % 5 > 0)
				t = count / 5 + 1;
			else t = count / 5;
			int showpage = 10;
			%><%if(n != 1)
		{%>
		<li><a href="AllDocItem?page=<%=n-1%>">上一页</a></li>
		<%} 
			if(n <= (showpage + 1) / 2 || t <= showpage){
            	for(i = 1; i <= showpage && i <= t; i++){
            		if(i == n){%>
                	<li><a><%=i%></a></li>
                	<%
                	}
            		else{
                	%>
            		<li><a href="AllDocItem?page=<%=i%>" ><%=i%></a></li> <%
            			}
            		}
        	}else{
            	%>
            	<li><a href="AllDocItem?page=1" >1...</a></li> <%
            	for( i = n - (showpage - 1) / 2 < t - (showpage - 1) ? n - (showpage - 1) / 2 : t - (showpage - 1); i <= n + showpage / 2 && i <= t; i++){
                if(i == n){%>
                	<li><a><%=i%></a></li>
                	<%
                	}
            		else{
                	%>
                <li><a href="AllDocItem?page=<%=i%>" ><%=i%></a></li> <%
                	}
                }
        	}
        	if(t - n >= showpage / 2 + 1 && t > showpage){
            	%>
            	<li><a href="AllDocItem?page=<%=t %>" >...<%=t %></a></li> <%
        	}%>
		<%if(n != t)
		{%>
		<li><a href="AllDocItem?page=<%=n+1%>">下一页</a></li>
		<%} %>
		</ul>
		<br>
		第<%=n%>页	共<%=t%>页
		</div>
<div id="item" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">订单详情</h3>
  </div>
  <div class="modal-body" id="itemDetail">
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">关闭</button>
  </div>
</div>

<div id="alert" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="true">
  	<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">提示</h3>
  </div>
  <div class="modal-body" id="msg">
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">关闭</button>
  </div>
</div>
</div>
  </body>
</html>
