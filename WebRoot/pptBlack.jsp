<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>



<!DOCTYPE HTML>
<html lang="en">
<head>
<meta charset="utf-8">
<%@ include file="../../WEB-INF/head.html" %>
<title>三边校园</title>
<!--
  <link rel="stylesheet" type="text/css" href="styles.css">
  -->
</head>

<body>
<%@ include file="../../WEB-INF/header.jsp" %>

  <div class="container" >
    <div class="row">
    <div class="span3 bs-docs-sidebar">
      <ul class="nav nav-list bs-docs-sidenav affix" style="width:190px;">
          <li ><a href="WordBlack.jsp"><i class="icon-chevron-right"></i> Word文档黑白打印</a></li>
          <li ><a href="WordColor.jsp"><i class="icon-chevron-right"></i> Word文档彩色打印</a></li>
          <li ><a href="ExcelBlack.jsp"><i class="icon-chevron-right"></i> Excel文档黑白打印</a></li>
          <li ><a href="ExcelColor.jsp"><i class="icon-chevron-right"></i> Excel文档彩色打印</a></li>
          <li ><a href="pptBlack.jsp"><i class="icon-chevron-right"></i> ppt文档黑白打印</a></li>
          <li><a href="pptColor.jsp"><i class="icon-chevron-right"></i> ppt文档彩色打印</a></li>
        </ul>
    </div>
    <div class="span9">
    <h2>PPT文档 黑白 打印下单</h2>
        <form method="post" action="ReceiveDocItem" enctype="multipart/form-data" id="itemForm">
          <input type="hidden" name="isColor" value="0">
          <input type="hidden" name="itemType" value="1">
          <input type="hidden" name="printType" value="2"> <!-- 这个参数是说明文档类型 1为doc -->
          <textarea name="otherRequest" style="display:none" id="otherRequest"></textarea>
    <table width="550">
        <tr>
          <td width="180">打印尺寸</td>
          <td style="width: 225px;">
            <select name="paperType" >
              <option value="A4">A4</option>
              <option value="B5">B5</option>
              <option value="A3">A3</option>
            </select>  
          </td>
        </tr>
        <tr>
          <td>打印纸张</td>
          <td>
            <select id="paper"> 
              <option value="普通">普通</option>
              <option value="红纸打印">红纸打印</option>
              <option value="奖状打印">奖状打印</option>
            </select>  
          </td>
        </tr>
        <tr>
          <td>单双面</td>
          <td>
            <label class="radio inline">
            <input type="radio" name="isSingle" value="1" checked="checked">单面
            </label>
            
            <label class="radio inline">
            <input type="radio" name="isSingle" value="0">双面 
            </label>
          </td>
        </tr>
        <tr>
          <td>打印份数：</td>
          <td><input type="text" name="copies" id="copies"></td>
          <td align="left"  id="copiesErr" style="color:red"></td>
        </tr>
<!--          <tr>
          <td>每份页数：</td>
          <td><input type="text" id="pages"></td>
          <td align="left"  id="pagesErr" style="color:red"></td>
        </tr> -->
         <tr>
          <td>每页幻灯片数：</td>
          <td>
          	<select name="ppts"> 
              <option value="1">1</option>
              <option value="6">6</option>
              <option value="8">8</option>
              <option value="9">9</option>
            </select>  
         </td>
           <td>
          	<select id="direction"> 
              <option value="纵向">纵向</option>
              <option value="横向">横向</option>
            </select>  
         </td>
        </tr>
        <tr>
          <td>文件上传：</td>
          <td><input type="file" name="docFile" id="upload" style="width:220px;" /></td>
          <td align="left" id="fileErr" style="color:red"></td>
        </tr>
        <tr>
        <td><input type="button" value="提交订单" class="btn" onclick="submitOne()"></td>
      </tr>
    </table> 
 	    <!-- 这是填真实信息的对话框  --> 
    <div id="TrueInf" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4>填写真实信息，以便取货时核对身份</h4>
      </div>
      <div class="modal-body">
        <table>
            <tr>
              <td>真实姓名:</td><td><input type="text" name="TrueName" id="TrueName" onblur="checkTrueName()" ></td><td><div class="error" id="TrueNameErr"></div></td>
            </tr>
            <tr>
              <td>联系电话:</td><td><input type="text" name="phone" id="phone" onblur="checkphone()"></td><td><div class="error" id="phoneErr"></div></td>
            </tr>
            <tr>
                  <td><input type="hidden" name="haveTrueName" value="1" id="haveTrueName"></td>
            </tr>
      </table>
      </div>
      <div class="modal-footer">
        <input type="button" class="btn btn-primary" onclick="submitTwo()" value="下一步">
      </div>
    </div>       
  </form>
  </div>
  </div>
  </div>
</body>
<script src="js/DocItemJs.js" type="text/javascript" charset="utf-8" async defer></script>
<script type="text/javascript" charset="utf-8" async defer>

  function addrequest()
  {
    var request="";
    request="页面方向："+$("#direction").val()+"<br>";
    
    
    $("#otherRequest").text(request);
    return true;

  }

function checkfile(){
  $("#fileErr").text("");
 var fileName = document.getElementById('upload').value;
 if(fileName==""){
    $("#fileErr").text("请选择文件名");
    document.getElementById('upload').focus();
    return false;
 }
 //lastIndexOf如果没有搜索到则返回为-1
    if(fileName.lastIndexOf(".")!=-1)
 {

    var fileType = (fileName.substring(fileName.lastIndexOf(".")+1,fileName.length)).toLowerCase();
       var suppotFile = new Array();
      suppotFile[0] = "ppt";
      suppotFile[1] = "pptx";
    for(var i =0;i<suppotFile.length;i++){
           if(suppotFile[i]==fileType){
         return true;
        }else{
       continue;
        }
  }
  $("#fileErr").text("不支持文件类型"+fileType+"，请正确选择文档类型");
  return false;
 }else
 {
  $("#fileErr").text("文件只支持主流文档格式");
  return false;
 }
}




function submitOne()
{
  addrequest();
  $('#TrueName').val(getCookie('TrueName'));
  $('#phone').val(getCookie('phone'));
  if(checkcopies()&&checkfile())  /* checkpages()&& */
  {
  if($('#TrueName').val()==""||$('#TrueName').val()=="\"\""||$('#phone').val()==""||$('#phone').val()=="\"\"")
    {
      
    $('#TrueName').val("");
    $('#phone').val("");
      $("#haveTrueName").val("0");
      $("#TrueInf").modal('show');
      
    }else
      {
      $("#itemForm").submit();
      }
  
  }
  
}

function submitTwo()
{
  if(checkTrueName()&&checkphone())
    {
      $("#itemForm").submit();
    }
}
</script>
</html>
