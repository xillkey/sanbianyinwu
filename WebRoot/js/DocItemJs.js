/*
   文档提交的检验函数

*/


function checkNum(s)
{
	var patrn=/^[0-9]{1,20}$/;
	if (!patrn.exec(s)) return false;
	return true;
}   

function isWhiteWpace (s)
{
	var whitespace = " \t\n\r";
	var i;
	for (i = 0; i < s.length; i++){   
		var c = s.charAt(i);
		if (whitespace.indexOf(c) >= 0) {
		  	return true;
		}
	}
	return false;
}

function checkpages()
{
	$("#pagesErr").text("");
	var pages=$.trim($("#pages").val());
	if(pages=="")
	{
		$("#pagesErr").text("请输入每份页数");
		return false;
	}else if(!checkNum(pages))
	{
		$("#pagesErr").text("请输入数字");
		return false;		
	}else
	{
		return true;
	}

}
function checkcopies()
{
  var copies = $.trim($("#copies").val());
  if(copies=="")
  {
    $('#copiesErr').text('请填写份数');
    return false;
  }else if(!checkNum(copies))
  {
    $('#copiesErr').text('请填写数字');
    return false;
  }else
  {
    $('#copiesErr').text("");
    return true;
  }

}

function checkphone()
{
	var phone = $.trim($("#phone").val());
	  if(phone=="")
	  {
	    $('#phoneErr').text('请填写联系方式');
	    return false;
	  }else if(!checkNum(phone))
	  {
	    $('#phoneErr').text('请填写数字');
	    return false;
	  }else
	  {
	    $('#phoneErr').text("");
	    return true;
	  }
}

function checkTrueName()
{
	var str = $.trim($("#TrueName").val());
	if(str == "")
	{
	 $('#TrueNameErr').text('请填写姓名');
	 return false;
	}else
	{
		$('#TrueNameErr').text("");
		return true;
	}
}




