<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="java.util.List"%>
<%@page import="com.sanbianyinwu.www.Public.DocItem"%>
<!DOCTYPE HTML>
<html lang="en">
  <head>
  <%@ include file="../../WEB-INF/head.html" %>
    <title>所要查询的订单</title>
  </head>
  <script type="text/javascript">
  $(document).ready(function(){
	  	$(".viewItem").click(
	  		  	function()
	  		  	{
	  		  		var itemID = $(this).attr("itemID");
	  		  		$.get("ViewDocItem", {itemID: itemID},
	  		  			   function(data){
	  		  			    $("#itemDetail").html(data);
	  		  			    $("#item").modal('show');
	  		  			   } 
	  		  			); 
	  		  	}
	  		  	
	  		  	);
	  	$(".cancelItem").click(
	  	function()
	  	{
		  		var itemID = $(this).attr("itemID");
  		  		$("#sure").attr("itemID",itemID);
  		  		$("#ask").modal("show");
	  	}
	  	);
	  	
	  	$("#sure").click(
	  			function()
	  		  	{		$("#ask").modal("hide");
	  			  		var itemID = $(this).attr("itemID");
	  	  		  		$.get("UserCancelDocItem", {itemID: itemID},
	  	  		  			   function(data){
	  	  		  			    $("#msg").html(data);
	  	  		  			    $("#alert").modal('show');
	  	  		  			    $("#alert").on("hidden",function()
	  	  		  			    {
	  	  		  			    	window.location.reload(); 
	  	  		  			    }		
	  	  		  			    );
	  	  		  			$("#sure").attr("itemID","");
	  	  		  			   } 
	  	  		  			); 
	  		  	}
	  	);
	  
  });

  </script>
  <body >
  <%@ include file="../../WEB-INF/header.jsp" %>
  <div class="container">
<h2>我的订单</h2>
  <table class="table table-hover" style="width:870px;text-align:center;margin:0 auto;">
	<tr>
				<th>
					订单类型
				</th>
				<th>
					订单号
				</th>
				<th>
					打印份数
				</th>
				<th>
					提交时间
				</th>
				<th>
					交易金额
				</th>
				<th>
					订单状态
				</th>
				<th style="width:204px;">
					操作
				</th>
			</tr>
			
			
			<%
				// 获取图书信息集合
				List<DocItem> list = (List<DocItem>) request.getAttribute("list");
				// 判断集合是否有效
				if (list == null || list.size() < 1) {
			%>
			<tr><td colspan="7">没有订单哦</td></tr>
			<%
				} else {
					// 遍历图书集合中的数据
					for (DocItem DI : list) {
			%>
			<tr align="center" bgcolor="white">
				<%
					String itemType;
					switch(DI.getitemType()){
					case(1):
						itemType = "个人";
						break;
					case(2):
						itemType = "班级";
						break;
					case(3):
						itemType = "社团";
						break;	
					default:
						itemType = "错误";
						break;
					}
					
					String itemStatus;
					switch(DI.getitemStatus()){
					case(1):
						itemStatus = "等待处理";
						break;
					case(2):
						itemStatus = "订单审核";
						break;
					case(3):
						itemStatus = "处理完成";
						break;
					case(4):
						itemStatus = "交易完成";
						break;
					case(5):
						itemStatus = "订单出错";
						break;
					case(6):
						itemStatus = "用户取消";
						break;
					default:
						itemStatus = "错误";
						break;
					}
				 %>
				<td><%=itemType%></td>
				<td><%=DI.getdocFile().substring(0, DI.getdocFile().lastIndexOf('.')) %></td>
				<td><%=DI.getcopies()%></td>
				<td><%=DI.getFormatsubTime()%></td>
				<td><%
				if(DI.getmoney()==0.0)out.print("未知");else out.print(DI.getmoney());
				%></td>
				<td><%=itemStatus%></td>
				<td>
				<p><button itemID="<%=DI.getitemID() %>" class="btn btn-primary viewItem">查看详情</button>
				<%if(DI.getitemStatus()==1){ %>
				<button class="btn btn-danger cancelItem" itemID="<%=DI.getitemID() %>" >取消订单</button>
				<%} %>
				</p>
				</td>
			</tr>
			<%
					}
				}
			%>
			
		</table>
		
		
		<div align="center">
		<%
			int t, i;
			int n = Integer.parseInt(request.getParameter("page") == null ? "1":request.getParameter("page")); 
			int count =(Integer) request.getAttribute("count");
			if(count % 5 > 0)
				t = count / 5 + 1;
			else t = count / 5;
			int showpage = 10;
			%><%if(n != 1)
		{%>
		<a href="MyDocItem?page=<%=n-1%>">上一页</a>
		<%} 
			if(n <= (showpage + 1) / 2 || t <= showpage){
            	for(i = 1; i <= showpage && i <= t; i++){
            		if(i == n)
            		{
            		%>
            		<%=i %>
            		<% }else{%>
            		<a href="MyDocItem?page=<%=i%>" ><%=i%></a> <%
            		}
            	}
        	}else{
            	%>
            	<a href="MyDocItem?page=1" >1...</a> <%
            	for( i = n - (showpage - 1) / 2 < t - (showpage - 1) ? n - (showpage - 1) / 2 : t - (showpage - 1); i <= n + showpage / 2 && i <= t; i++){
                if(i == n)
            		{
            		%>
            		<%=i %>
            		<% }else{%>
                <a href="MyDocItem?page=<%=i%>" ><%=i%></a> <%
                	}
                }
        	}
        	if(t - n >= showpage / 2 + 1 && t > showpage){
            	%>
            	<a href="MyDocItem?page=<%=t %>" >...<%=t %></a> <%
        	}%>
		<%if(n != t)
		{%>
		<a href="MyDocItem?page=<%=n+1%>">下一页</a>
		<%} %>
		<br>
		第<%=n%>页	共<%=t%>页
		</div>
	</div>
	<div id="item" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="true">
	  <div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h3 id="myModalLabel">订单详情</h3>
	  </div>
	  <div class="modal-body" id="itemDetail">
	  </div>
	  <div class="modal-footer">
	    <button class="btn" data-dismiss="modal" aria-hidden="true">关闭</button>
	  </div>
	</div>
	<div id="alert" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="true">
	  <div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h3 id="myModalLabel">温馨提示</h3>
	  </div>
	  <div class="modal-body" id="msg"></div>
	  <div class="modal-footer">
	    <button class="btn" data-dismiss="modal" aria-hidden="true">关闭</button>
	  </div>
	</div>
	<div id="ask" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="true">
	  <div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h3 id="myModalLabel">温馨提示</h3>
	  </div>
	  <div class="modal-body" id="askmsg">你确定取消该订单？</div>
	  <div class="modal-footer">
	  	<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" id="sure">确定</button>
	    <button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
	  </div>
	</div>
  </body>


</html>
