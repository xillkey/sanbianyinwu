 <%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
 <%@ page  import="com.sanbianyinwu.www.User.login.UserFile" %>
 <%
 	UserFile UF = (UserFile)request.getAttribute("UserFile");
 %>
<!DOCTYPE HTML>
<html lang="en">
<head>
<meta charset="utf-8">
<%@ include file="../../WEB-INF/head.html" %>
<title>用户注册页面</title>
<link REL=stylesheet href="css/register.css" type="text/css">
<script type="text/javascript" src="js/register.js"></script>
</head>
<body>
<%@ include file="../../WEB-INF/header.jsp" %>
<div class="container" >
	<Form name="formRegister" method="post" id="userInf">
	<table style="text-align:right; margin:0px auto;width:950px">
	
	<tr align="center">
		<td colspan="2" style="font-size: 20px;">基本信息</td>
	</tr>
	<tr>
        <td>用户名（登陆用）: </td><td><input type="text" name="userName" readonly="readonly" value="<%=UF.getUserName() %>"></td>
		<td><div id="usernameErr" class="error" style="width:340px;"></div></td>
	</tr>	
    <tr>    
        <td>电子邮箱:</td><td><input type="text" name="email" readonly="readonly" value="<%=UF.getEmail() %>"></td>
        <td><div id="emailErr" class="error"></div></td>
    </tr>
    <tr>    
        <td>学院:</td>
        <td>
        <input type="text" readonly="readonly" id="collegeBox" name="college" value="<%=UF.getCollege() %>">
        <select  onBlur="Checkcollege(this.value);$('#collegeBox').val(this.value);">
			<option value="<%=UF.getCollege() %>" >修改学院</option>
			<option value="农学院">农学院</option>
			<option value="资源环境学院">资源环境学院</option>
			<option value="生命科学学院">生命科学学院</option>
			<option value="经济管理学院">经济管理学院</option>
			<option value="工程学院">工程学院</option>
			<option value="动物科学学院">动物科学学院</option>
			<option value="兽医学院 ">兽医学院</option>
			<option value="园艺学院">园艺学院</option>
			<option value="食品学院">食品学院</option>
			<option value="林学院">林学院</option>
			<option value="人文与法学学院">人文与法学学院</option>
			<option value="理学院">理学院</option>
			<option value="信息学院">信息学院</option>
			<option value="软件学院">软件学院</option>
			<option value="艺术学院">艺术学院</option>
			<option value="外国语学院">外国语学院</option>
			<option value="水利与土木工程学院">水利与土木工程学院</option>
			<option value="公共管理学院">公共管理学院</option>
			<option value="继续教育学院">继续教育学院</option>
			<option value="国际教育学院">国际教育学院</option>
		</select></td> 
         <td><div id="collegeErr" class="error"></div></td>
    </tr>    
    <tr>
		<td>年级:</td>
		<td>
		<input type="text" readonly="readonly" id="yearBox" value="<%=UF.getYear() %>" name="year">
		<select  onBlur="Checkyear(this.value);$('#yearBox').val(this.value);" id="year">
								<option value="<%=UF.getYear() %>" >修改年级</option>
      	 					</select> </td>
						<td><div id="yearErr" class="error"></div></td>
    	</tr>
         <tr>
        <td>宿舍区:</td>
        <td>
        <input type="text" name="address" readonly="readonly" id="addressBox" value="<%=UF.getAddress() %>">
        <select onblur="$('#addressBox').val(this.value)">
        <option value="<%=UF.getAddress() %>">修改宿舍区</option>
        <option value="五山(泰山)">五山(泰山)</option>
        <option value="华山">华山</option>
        <option value="跃进南">跃进南</option>
        <option value="跃进北">跃进北</option>
        </select>
        </td>
    </tr> 
    <tr>    
        <td>专业:</td><td><input type="text" name="major" onBlur="Checkmajor(this.value)" value="<%=UF.getMajor() %>"></td> 
        <td></td>
    </tr>
    <tr> 
        <td>学号:</td><td><input type="text" name="studentNum" onBlur="checkstudentNum(this.value.toLowerCase())" value="<% if(UF.getStudentNum()==null)
        	{%><%}else out.print(UF.getStudentNum());%>"><div id="studentNumErr"></div></td>
        <td></td>
    </tr>
    <tr>    
        <td>真实姓名:</td><td><input type="text" name="TrueName" value="<%=UF.getTrueName() %>"></td>
    </tr>
    <tr>    
        <td>长号电话:</td><td><input type="text" name="longNum" onBlur="Checklongnum(this.value)" value="<%=UF.getLongNum() %>"><div id="longNumErr"></div></td> 
        <td></td>
    </tr>   
    <tr>
        <td>短号电话:</td><td><input type="text" name="shortNum" onBlur="Checkshortnum(this.value)" value="<%=UF.getShortNum() %>"><div id="shortNumErr"></div></td>
        <td></td>
	</tr>
	<tr>
        <td>QQ:</td><td><input type="text" name="qq" onBlur="Checkqq(this.value)" value="<%=UF.getQq() %>"><div id="qqErr"></div></td>
        <td></td>
     </tr>


    <tr>     
		<td colspan="2" align="center"><input type="button" value="提交" onclick="ajaxSubmit()" style="float:none;" class="btn btn-primary"></td>
	</tr>
	
		</table>
		</Form>
	</div>
	<div id="alert" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-header">
		    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		    <h3 id="myModalLabel">信息</h3>
		  </div>
		  <div class="modal-body" id="msg">
		  </div>
		  <div class="modal-footer">
		    <button class="btn" data-dismiss="modal" aria-hidden="true">关闭</button>
		  </div>
</div>
</body>
      	<script type="text/javaScript">
      	  creatdate();
      	  
      	  function ajaxSubmit()
      	  {
      		  if(checkEditdata())
  			  {
      			$.ajax({
                    cache: true,
                    type: "POST",
                    url:"UpdateUserInf",
                    data:$('#userInf').serialize(),
                    async: false,
                    error: function(request) {
                        alert("网络超时，请稍后再试");
                    },
                    success: function(data) {
                        $("#msg").html(data);
                        $("#alert").modal('show');
                        $("#alert").on('hidden',function()
                        {
                        	window.location.reload();
                        }		
                        );
                    }
                });
  			  }
      	  }
      	</script> 
</html>
