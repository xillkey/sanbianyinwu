package com.sanbianyinwu.www.User.login;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mysql.jdbc.EscapeTokenizer;
import com.sanbianyinwu.www.Public.Function_Library;
import com.sanbianyinwu.www.Public.JDBConnection;

/**
 * Servlet implementation class EditUserInf
 */
//@WebServlet("/EditUserInf")
public class EditUserInf extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditUserInf() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Cookie[] cookies = request.getCookies();
		Cookie userIDcookie = Function_Library.findCookie(cookies, "LoginID");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		if(userIDcookie!=null&&(userIDcookie.getValue()!=null&&!"".equals(userIDcookie.getValue())))
		{
			String userID = userIDcookie.getValue();
			int ID = Integer.parseInt(userID);
			UserFile UF = new UserFile();
			UF = DBconnect.GetUserFile(ID);
			response.setCharacterEncoding("UTF-8");
			request.setAttribute("UserFile", UF);
			request.getRequestDispatcher("user/editUserFile.jsp").forward(request, response);
			
		}else {
			response.getWriter().print("登陆超时");
		}
	}

}
