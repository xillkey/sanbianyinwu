package com.sanbianyinwu.www.User.login;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import oracle.jrockit.jfr.tools.ConCatRepository;

import com.sanbianyinwu.www.Public.Function_Library;
import com.sanbianyinwu.www.Public.JDBConnection;

//import sun.org.mozilla.javascript.internal.ast.NewExpression;

/**
 * Servlet implementation class UdateUserInf
 */
//@WebServlet("/UdateUserInf")
public class UpdateUserInf extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateUserInf() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		UserFile UF = new UserFile();
		String studentNum = null;
		if(!"".equals(request.getParameter("studentNum")))
			studentNum = request.getParameter("studentNum");
		UF.setStudentNum(studentNum);
		UF.setTrueName(request.getParameter("TrueName"));
		UF.setQq(request.getParameter("qq"));
		UF.setLongNum(request.getParameter("longNum"));
		UF.setShortNum(request.getParameter("shortNum"));
		UF.setAddress(request.getParameter("address"));
		UF.setCollege(request.getParameter("college"));
		UF.setMajor(request.getParameter("major"));
		UF.setYear(Integer.parseInt(request.getParameter("year")));
		Cookie[] cookies = request.getCookies();
		Cookie userIDCookie = Function_Library.findCookie(cookies, "LoginID");
		Cookie trueNameCookie = Function_Library.findCookie(cookies, "TrueName");
		Cookie phoneCookie = Function_Library.findCookie(cookies, "phone");
		
		trueNameCookie.setValue(URLEncoder.encode(UF.getTrueName(),"UTF-8"));
		phoneCookie.setValue(UF.getLongNum());
		response.addCookie(phoneCookie);
		response.addCookie(trueNameCookie);
		
		JDBConnection con = new JDBConnection(); 
		if(userIDCookie!=null&&userIDCookie.getValue()!=null)
		{
			String sql = "update yx_user set studentNum = ? ,TrueName = ? ,qq = ? ,longNum = ?,shortNum = ?,address = ?,college = ?,major = ? ,year= ? where userID = "+userIDCookie.getValue();
			PreparedStatement ps = con.CreatePreparedStatement(sql);
			try {
				if("".equals(UF.getStudentNum()))
					ps.setString(1,null);
				else
					ps.setString(1, UF.getStudentNum());
				ps.setString(2, UF.getTrueName());
				ps.setString(3, UF.getQq());
				ps.setString(4, UF.getLongNum());
				ps.setString(5, UF.getShortNum());
				ps.setString(6, UF.getAddress());
				ps.setString(7, UF.getCollege());
				ps.setString(8, UF.getMajor());
				ps.setInt(9, UF.getYear());
				if(ps.executeUpdate()>0)
				{
					out.print("修改成功");
				}
				ps.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			con.closeConnection();
			
			
		}
		
	}

}
