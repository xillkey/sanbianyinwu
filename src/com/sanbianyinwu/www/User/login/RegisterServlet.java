package com.sanbianyinwu.www.User.login;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {		//注册操作,返回到是否成功的页面
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("UTF-8");
		//PrintWriter out = response.getWriter();
		UserFile RF = new UserFile(request);
		int row = DBconnect.DBUserRegister(RF);
		if(row == 0){	//插入注册表失败后,row=0,其实只有row=1或者0两种状况
			request.setAttribute("error", "注册失败");
			request.getRequestDispatcher("WEB-INF/err.jsp").forward(request, response);
			//out.print("对不起，注册失败");
		}
		else{
			//request.getSession(true).setAttribute("userName", RF.getUserName());
			//request.getSession(true).setAttribute("password", RF.getPassword());
			request.setAttribute("error", "注册成功");
			request.getRequestDispatcher("WEB-INF/err.jsp").forward(request, response);
			//out.print("注册成功");
			
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}

}
