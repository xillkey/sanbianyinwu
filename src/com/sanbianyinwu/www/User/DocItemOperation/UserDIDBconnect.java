package com.sanbianyinwu.www.User.DocItemOperation;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sanbianyinwu.www.Public.JDBConnection;
import com.sanbianyinwu.www.Public.DocItem;


public class UserDIDBconnect extends JDBConnection {
	
	public static int DocItemAddItem(DocItem DI){
		int row = 0;
		JDBConnection con=new JDBConnection();
		String sql="insert into yx_docitem(itemType,printType,userID,adminID,studentNum,TrueName,paperType," +
					"copies,isColor,isSingle,ppts,otherRequest,itemStatus,docFile,subTime,takeTime,money)" +
					" values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement ps = con.CreatePreparedStatement(sql);
		try{	//获取信息
			ps.setInt(1,DI.getitemType());
			ps.setInt(2,DI.getprintType());
			ps.setInt(3,DI.getuserID());
			ps.setInt(4,DI.getadminID());
			ps.setString(5,DI.getstudentNum());
			ps.setString(6,DI.getTrueName());
			ps.setString(7,DI.getpaperType());
			ps.setInt(8,DI.getcopies());
			ps.setInt(9,DI.getisColor());
			ps.setInt(10,DI.getisSingle());
			ps.setInt(11,DI.getppts());
			ps.setString(12,DI.getotherRequest());
			ps.setInt(13,DI.getitemStatus());
			ps.setString(14,DI.getdocFile());
			ps.setLong(15,DI.getSubTime());
			ps.setLong(16,DI.getTakeTime());
			ps.setFloat(17,DI.getmoney());
			row = ps.executeUpdate();
			if(row > 0)
				System.out.println("成功添加了" + row + "条数据");
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			try {
				ps.close();
			} catch (SQLException e) {
			e.printStackTrace();
			}
		}
		con.closeConnection();
		return row;
	}
	
	public static List<DocItem> GetDocItem(int userID,int page){
		JDBConnection con = new JDBConnection();
		Integer pagestart = (page - 1) * 5;
		String sql = "select * from yx_docitem WHERE userID = "+ ((Integer)userID).toString() + " order by subTime desc limit " + pagestart.toString() + ", 5";
		ResultSet rs = con.executeQuery(sql);
		List <DocItem> L = new ArrayList<DocItem>();
		try{
			while(rs.next()){
				DocItem DI = new DocItem();
				DI.setitemID(rs.getInt("itemID"));
				DI.setitemType(rs.getInt("itemType"));
				DI.setprintType(rs.getInt("printType"));
				DI.setuserID(rs.getInt("userID"));
				DI.setadminID(rs.getInt("adminID"));
				DI.setstudentNum(rs.getString("studentNum"));
				DI.setTrueName(rs.getString("TrueName"));
				DI.setpaperType(rs.getString("paperType"));
				DI.setcopies(rs.getInt("copies"));
				DI.setisColor(rs.getInt("isColor"));
				DI.setisSingle(rs.getInt("isSingle"));
				DI.setppts(rs.getInt("ppts"));
				DI.setotherRequest(rs.getString("otherRequest"));
				DI.setitemStatus(rs.getInt("itemStatus"));
				DI.setdocFile(rs.getString("docFile"));
				DI.setSubTime(rs.getLong("SubTime")); 
				DI.setTakeTime(rs.getLong("takeTime"));
				DI.setmoney(rs.getFloat("money"));
				L.add(DI);
			}
			rs.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
		con.closeConnection();
		return L;
	}
	
	
	public static int getCount(int userID) {
		JDBConnection con = new JDBConnection();
		String sql = "select count(*) from yx_docitem where userID = " + ((Integer)userID).toString();
		ResultSet rs = con.executeQuery(sql);
		int temp = 0;
		try {
			rs.next();
			temp = rs.getInt(1);
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		con.closeConnection();
		return temp;
	}
	
	 public static int cancelItem(int itemID){
		 JDBConnection con = new JDBConnection();
		 String sql ="update yx_docitem set itemStatus=6 where itemID = "+itemID+" and itemStatus =1";
		 int row = con.executeUpdate(sql);
		 return row;
	 }
}

