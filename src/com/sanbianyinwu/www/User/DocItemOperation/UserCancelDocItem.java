package com.sanbianyinwu.www.User.DocItemOperation;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class UserCancelDocItem
 */
//@WebServlet("/UserCancelDocItem")
public class UserCancelDocItem extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserCancelDocItem() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String itemIDsString = request.getParameter("itemID");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=UTF-8");
		if(itemIDsString!=null)
		{
			int itemID = Integer.parseInt(itemIDsString);
			int row = UserDIDBconnect.cancelItem(itemID);
			if(row<1)
			{
				response.getWriter().print("操作出错");
			}else
			{		
				response.getWriter().print("操作成功");
			}
		}else
		{		
			response.getWriter().print("操作出错");
		}
	}

}
