package com.sanbianyinwu.www.User.DocItemOperation;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sanbianyinwu.www.Public.Function_Library;
import com.sanbianyinwu.www.Public.DocItem;

public class UserGetDocItem extends HttpServlet {
	private static final long serialVersionUID = 1L;


	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		Cookie[] userCookie=request.getCookies();
		int userID = Integer.parseInt( Function_Library.findCookie(userCookie, "LoginID").getValue() );
		int count = UserDIDBconnect.getCount(userID);
		
		int page=Integer.parseInt(request.getParameter("page") == null ? "1" 
                : request.getParameter("page")); 
	
		List<DocItem> list = UserDIDBconnect.GetDocItem(userID,page);
		request.setAttribute("count",count);
		request.setAttribute("list", list);
		request.getRequestDispatcher("user/MyDocItem.jsp").forward(request, response);
	}


	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(req, resp);
	}

}
