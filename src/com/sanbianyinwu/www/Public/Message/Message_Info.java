package com.sanbianyinwu.www.Public.Message;

import java.text.SimpleDateFormat;

public class Message_Info {
	private int messageID;	//留言号
	private int userID;
	private String userName;
	private int adminID;	
	private String adminName;
	private String message;	//留言信息
	private long subtime;	//提交时间
	private int have_read;	//是否已经被阅读了（0为未读，1为已读）
	private int mark;		//标识（0为未被标记,1为被标记）
	
	public String getFormatsubTime() {			//按格式取日期
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		java.sql.Timestamp subTime = new java.sql.Timestamp(this.subtime);
		String str = df.format(subTime);
		return str;
	}
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getAdminName() {
		return adminName;
	}
	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}
	public int getMessageID() {
		return messageID;
	}
	public void setMessageID(int messageID) {
		this.messageID = messageID;
	}
	public int getUserID() {
		return userID;
	}
	public void setUserID(int userID) {
		this.userID = userID;
	}
	public int getAdminID() {
		return adminID;
	}
	public void setAdminID(int adminID) {
		this.adminID = adminID;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public long getSubtime() {
		return subtime;
	}
	public void setSubtime(long subtime) {
		this.subtime = subtime;
	}
	public int getHave_read() {
		return have_read;
	}
	public void setHave_read(int have_read) {
		this.have_read = have_read;
	}
	public int getMark() {
		return mark;
	}
	public void setMark(int mark) {
		this.mark = mark;
	}
		
}


class MS_attribute {
	public int user_type;
	//user_type
	//用户(1)
	//管理员(2)
	//全部都看(3)
	//用户个人(4)
	//管理员个人（5）
	//切勿写其它
	public Integer id = null;
	//当要查个人信息是才起作用
	//不需要查的时候可以不写
	public int print_type;
	//print_type表示输出的内容
	//输出全部的记录（1）
	//输出没被读过的记录(2)
	//输出已经读过的记录(3)
	//输出被标记的记录(4)
	public int page;
	//显示页数,当limit为false时失效
	public boolean limit;
	//是否限制条数
	
	MS_attribute(){
		user_type = 3;
		print_type = 1;
		page = 1;
		limit = true;
	}
	
	MS_attribute(int user_type,int print_type,int page,boolean limit){
		this.user_type = user_type;
		this.print_type = print_type;
		this.page = page;
		this.limit = limit;
	}
}