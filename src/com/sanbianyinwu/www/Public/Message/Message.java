package com.sanbianyinwu.www.Public.Message;

import java.awt.print.Pageable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sanbianyinwu.www.Public.Function_Library;
import com.sanbianyinwu.www.Public.Message.Message_Info;

/**
 * Servlet implementation class Message
 */
//@WebServlet("/Message")
public class Message extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private int totalpage = 1;
	private String action = null;
	private String show = null;
	private String page = null;
	private int MessageID;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Message() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		 action = request.getParameter("action");
		 show = request.getParameter("show");
		 page = request.getParameter("page");
			if(page==null) page = "1";
		if(action==null)
		{
			action="myMessages";
		}
		
		if("submitMessage".equals(action))
		{
			submitMessage(request, response);
		}else if("myMessages".equals(action))
		{
			myMessages(request, response);
		}else if("adminMessages".equals(action))
		{
			adminMessages(request, response);
		}else if("userMessages".equals(action))
		{
			userMessages(request, response);
		}else if("viewMessage".equals(action))
		{
			viewMessage(request, response);
		}else  if("allMessages".equals(action))
		{
			allMessages(request, response);
		}else if("markMessage".equals(action)){
			markMessage(request, response);
		}
		
		
	}
	private void submitMessage(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		request.setCharacterEncoding("UTF-8");
		String message = request.getParameter("message");
		Cookie[] cookies = request.getCookies();
		Cookie typeCookie = Function_Library.findCookie(cookies, "type");
		Cookie IDcookie = Function_Library.findCookie(cookies, "LoginID");
		Message_Info MI = new Message_Info(); 
		MI.setMessage(message);
		MI.setSubtime(System.currentTimeMillis());
		if(typeCookie != null && !"".equals(typeCookie.getValue()))
		{
			if("admin".equals(typeCookie.getValue()))
			{	//提交者是管理员
				MI.setAdminID(Integer.parseInt(IDcookie.getValue()));
				
			}else if("user".equals(typeCookie.getValue())){
				//提交者是用户
				MI.setUserID(Integer.parseInt(IDcookie.getValue()));
			}
			
			if(MsDBconnect.CreateMs(MI)>0)
			{
				try {
					response.getWriter().print("提交留言成功");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}else {
			
				//response.getWriter().print("请先登录");
				request.setAttribute("error", "请先登录.");
				request.getRequestDispatcher("/WEB-INF/err.jsp").forward(request, response);
		
		      }
		
				
	}
	
	private void myMessages(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		Cookie[] cookies = request.getCookies();
		Cookie typeCookie = Function_Library.findCookie(cookies, "type");
		Cookie IDcookie = Function_Library.findCookie(cookies, "LoginID");

		MS_attribute Ma = new MS_attribute();
		//String show = request.getParameter("show");
		
		if(typeCookie != null && !"".equals(typeCookie.getValue()))
		{
			if("user".equals(typeCookie.getValue()))
			{
				Ma.user_type=4;
			}else if("admin".equals(typeCookie.getValue())) {
				Ma.user_type=5;
			}
				Ma.id=Integer.parseInt(IDcookie.getValue());
				if("haveread".equals(show))
				{
					Ma.print_type = 3;
				}else if("havenotread".equals(show))
				{
					Ma.print_type = 2;
				}else if("marked".equals(show))
				{
					Ma.print_type = 4;
				}else {
					Ma.print_type = 1;
					show = "all";
				}
				Ma.page =  Integer.parseInt(page);
				List <Message_Info> msList = MsDBconnect.RetrieveMS(Ma);
				totalpage = MsDBconnect.RetrieveMS_Count(Ma, 10);
				/*request.setAttribute("show", show);
				request.setAttribute("action", action);*/
				request.setAttribute("query", queryBuilder());
				request.setAttribute("totalpage", totalpage);				
				request.setAttribute("msList", msList);
				request.setAttribute("type", typeCookie.getValue());

				request.getRequestDispatcher("WEB-INF/msglist.jsp").forward(request, response);
			
		}
	}
	
	private void adminMessages(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{

		Cookie[] cookies = request.getCookies();
		Cookie typeCookie = Function_Library.findCookie(cookies, "type");
		//String page = request.getParameter("page");
		//if(page==null) page = "1";
		MS_attribute Ma = new MS_attribute();
		//String show = request.getParameter("show");
		if(typeCookie != null && "admin".equals(typeCookie.getValue())) //管理员才有权限
		{
			Ma.user_type = 2;
			if("haveread".equals(show))
			{
				Ma.print_type = 3;
			}else if("havenotread".equals(show))
			{
				Ma.print_type = 2;
			}else if("marked".equals(show))
			{
				Ma.print_type = 4;
			}else {
				Ma.print_type = 1;
				show = "all";
			}
			Ma.page =  Integer.parseInt(page);
			List <Message_Info> msList = MsDBconnect.RetrieveMS(Ma);
			totalpage = MsDBconnect.RetrieveMS_Count(Ma, 10);
			request.setAttribute("totalpage", totalpage);	
			/*	request.setAttribute("show", show);
			request.setAttribute("action", action);*/
			request.setAttribute("query", queryBuilder());
			request.setAttribute("msList", msList);
			request.setAttribute("type", typeCookie.getValue());
			request.getRequestDispatcher("WEB-INF/msglist.jsp").forward(request, response);
		
		}
	}
	private void userMessages(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{

		Cookie[] cookies = request.getCookies();
		Cookie typeCookie = Function_Library.findCookie(cookies, "type");
		//String page = request.getParameter("page");
		
		//if(page==null) page = "1";
		MS_attribute Ma = new MS_attribute();
		//String show = request.getParameter("show");
		if(typeCookie != null && "admin".equals(typeCookie.getValue())) //管理员才有权限
		{
			Ma.user_type = 1;
			if("haveread".equals(show))
			{
				Ma.print_type = 3;
			}else if("havenotread".equals(show))
			{
				Ma.print_type = 2;
			}else if("marked".equals(show))
			{
				Ma.print_type = 4;
			}else {
				Ma.print_type = 1;
				show = "all";
			}
			Ma.page =  Integer.parseInt(page);
			
			List <Message_Info> msList = MsDBconnect.RetrieveMS(Ma);
			totalpage = MsDBconnect.RetrieveMS_Count(Ma, 10);
			/*request.setAttribute("show", show);
			request.setAttribute("action", action);*/
			request.setAttribute("query", queryBuilder());
			request.setAttribute("totalpage", totalpage);	
			request.setAttribute("msList", msList);
			request.setAttribute("type", typeCookie.getValue());
			request.getRequestDispatcher("WEB-INF/msglist.jsp").forward(request, response);
		
		}
	}
	
	private void allMessages(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{

		Cookie[] cookies = request.getCookies();
		Cookie typeCookie = Function_Library.findCookie(cookies, "type");
		//String page = request.getParameter("page");
		//if(page==null) page = "1";
		MS_attribute Ma = new MS_attribute();
		//String show = request.getParameter("show");
		if(typeCookie != null && "admin".equals(typeCookie.getValue())) //管理员才有权限
		{
			Ma.user_type = 3;
			if("haveread".equals(show))
			{
				Ma.print_type = 3;
			}else if("havenotread".equals(show))
			{
				Ma.print_type = 2;
			}else if("marked".equals(show))
			{
				Ma.print_type = 4;
			}else {
				Ma.print_type = 1;
				show = "all";
			}
			Ma.page =  Integer.parseInt(page);
			List <Message_Info> msList = MsDBconnect.RetrieveMS(Ma);
			totalpage = MsDBconnect.RetrieveMS_Count(Ma, 10);
			request.setAttribute("totalpage", totalpage);
			/*request.setAttribute("show", show);
			request.setAttribute("action", action);*/
			request.setAttribute("query", queryBuilder());
			request.setAttribute("msList", msList);
			request.setAttribute("type", typeCookie.getValue());
			request.getRequestDispatcher("WEB-INF/msglist.jsp").forward(request, response);
		
		}
	}
	
	private void viewMessage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		MessageID = request.getParameter("messageID")==null?-1:Integer.parseInt(request.getParameter("messageID"));
		if(MessageID!=-1)
		{
			Message_Info MI = new Message_Info(); 
			MI = MsDBconnect.SelectMs(MessageID);
			if(MI.getHave_read()==0){ //未读才更新数据库
			
				MsDBconnect.UpdateMS_Have_Read(MessageID, true);
			
			}
			request.setAttribute("MI", MI);
			request.getRequestDispatcher("WEB-INF/msg.jsp").forward(request, response);
		}else {
			response.getWriter().print("出错");
		}
	}
	
	private void markMessage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		MessageID = request.getParameter("messageID")==null?-1:Integer.parseInt(request.getParameter("messageID"));
		if(MessageID!=-1)
		{
			Message_Info MI = new Message_Info(); 
			MI = MsDBconnect.SelectMs(MessageID);
			if(MI.getMark()==0){ //未mark才更新数据库
			
				MsDBconnect.UpdateMS_Mark(MessageID, 1);
				response.getWriter().print("点赞成功");
			}else {
				response.getWriter().print("请勿重复点赞");
			}
			
		}else {
			response.getWriter().print("出错");
		}
	}
	
	private String queryBuilder()
	{
		String query = "";
		if(action!=null&&show!=null)
		query = "?action="+action+"&show="+show;
		return query;
	}
	
}
