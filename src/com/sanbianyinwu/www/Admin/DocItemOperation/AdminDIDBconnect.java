package com.sanbianyinwu.www.Admin.DocItemOperation;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sanbianyinwu.www.Public.JDBConnection;
import com.sanbianyinwu.www.Public.DocItem;

public class AdminDIDBconnect extends JDBConnection {

	public static int DocItemUpdate(int itemID ,int adminID){
		int flag = -1;
		JDBConnection con=new JDBConnection();
		String sql = "SELECT * FROM yx_docitem WHERE itemID='" + ((Integer)itemID).toString() + "'";
		ResultSet rs = con.executeQuery(sql);
		String sql2 = "update yx_docitem set adminID=? WHERE itemID=?";
		PreparedStatement ps = con.CreatePreparedStatement(sql2);
		try{	
			if(rs.getString("adminID") != null)
				flag = 0;
			else{
				ps.setInt(1, adminID);
				ps.setInt(2, itemID);
				ps.executeUpdate();
				flag = 1;
			}
		}catch(SQLException e){
			e.printStackTrace();
			flag = -1;
		}finally{
			try {
				rs.close();
				ps.close();
			} catch (SQLException e) {
			e.printStackTrace();
			}
		}
		con.closeConnection();
		return flag;	//1为成功，0为已经被接单，-1操作不成功
	}
	
	public static List<DocItem> GetDocItem(int userID,int page){
		JDBConnection con = new JDBConnection();
		Integer pagestart = (page - 1) * 5;
		String sql = "select * from yx_docitem WHERE userID = "+ ((Integer)userID).toString() + " order by subTime desc limit " + pagestart.toString() + ", 5";
		ResultSet rs = con.executeQuery(sql);
		List <DocItem> L = new ArrayList<DocItem>();
		try{
			while(rs.next()){
				DocItem DI = new DocItem();
				DI.setitemID(rs.getInt("itemID"));
				DI.setitemType(rs.getInt("itemType"));
				DI.setprintType(rs.getInt("printType"));
				DI.setuserID(rs.getInt("userID"));
				DI.setadminID(rs.getInt("adminID"));
				DI.setstudentNum(rs.getString("studentNum"));
				DI.setTrueName(rs.getString("TrueName"));
				DI.setpaperType(rs.getString("paperType"));
				DI.setcopies(rs.getInt("copies"));
				DI.setisColor(rs.getInt("isColor"));
				DI.setisSingle(rs.getInt("isSingle"));
				DI.setppts(rs.getInt("ppts"));
				DI.setotherRequest(rs.getString("otherRequest"));
				DI.setitemStatus(rs.getInt("itemStatus"));
				DI.setdocFile(rs.getString("docFile"));
				DI.setSubTime(rs.getLong("SubTime")); 
				DI.setTakeTime(rs.getLong("takeTime"));
				DI.setmoney(rs.getFloat("money"));
				L.add(DI);
			}
			rs.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
		con.closeConnection();
		return L;
	}
	
	
	public static int getCount(int userID) {
		JDBConnection con = new JDBConnection();
		String sql = "select count(*) from yx_docitem where userID = " + ((Integer)userID).toString();
		ResultSet rs = con.executeQuery(sql);
		int temp = 0;
		try {
			rs.next();
			temp = rs.getInt(1);
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		con.closeConnection();
		return temp;
	}
	
	public static List<DocItem> getAllDocItem(int page){
		JDBConnection con = new JDBConnection();
		Integer pagestart = (page - 1) * 5;
		String sql = "select * from yx_docitem order by subTime desc limit " + pagestart.toString() + ", 5";
		ResultSet rs = con.executeQuery(sql);
		List <DocItem> L = new ArrayList<DocItem>();
		try{
			while(rs.next()){
				DocItem DI = new DocItem();
				DI.setitemID(rs.getInt("itemID"));
				DI.setitemType(rs.getInt("itemType"));
				DI.setprintType(rs.getInt("printType"));
				DI.setuserID(rs.getInt("userID"));
				DI.setadminID(rs.getInt("adminID"));
				DI.setstudentNum(rs.getString("studentNum"));
				DI.setTrueName(rs.getString("TrueName"));
				DI.setpaperType(rs.getString("paperType"));
				DI.setcopies(rs.getInt("copies"));
				DI.setisColor(rs.getInt("isColor"));
				DI.setisSingle(rs.getInt("isSingle"));
				DI.setppts(rs.getInt("ppts"));
				DI.setotherRequest(rs.getString("otherRequest"));
				DI.setitemStatus(rs.getInt("itemStatus"));
				DI.setdocFile(rs.getString("docFile"));
				DI.setSubTime(rs.getLong("SubTime")); 
				DI.setTakeTime(rs.getLong("takeTime"));
				DI.setmoney(rs.getFloat("money"));
				L.add(DI);
			}
			rs.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
		con.closeConnection();
		return L;
	}
	
	public static int getAllCount() {
		JDBConnection con = new JDBConnection();
		String sql = "select count(*) from yx_docitem ";
		ResultSet rs = con.executeQuery(sql);
		int temp = 0;
		try {
			rs.next();
			temp = rs.getInt(1);
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		con.closeConnection();
		return temp;
	}
	
}


 
