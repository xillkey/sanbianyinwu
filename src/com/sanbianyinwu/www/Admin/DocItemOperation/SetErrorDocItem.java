package com.sanbianyinwu.www.Admin.DocItemOperation;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sanbianyinwu.www.Public.JDBConnection;

/**
 * Servlet implementation class SetErrorDocItem
 */
//@WebServlet("/SetErrorDocItem")
public class SetErrorDocItem extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SetErrorDocItem() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
			response.setContentType("text/html;charset=UTF-8");
	String itemID = request.getParameter("itemID");
	PrintWriter out = response.getWriter();
	      if(itemID == null)
	      {
	    	  out.print("错误啦");
	    	  
	      }else {
			JDBConnection con = new JDBConnection();
			ResultSet itemStastus ;
			try {
				itemStastus = con.executeQuery("select itemStatus from yx_docitem where itemID="+itemID);
				if(itemStastus.next())
				{
					switch (itemStastus.getInt(1)) {
					case 1:out.print("无法完成操作,因为该订单还未审核");
						
						break;
					case 2:	
							if(con.executeUpdate("update yx_docitem set itemStatus = 5 where itemID = "+itemID)>0)
								out.print("操作成功，该订单的状态已设成 <font color='red'>订单出错</font>");
					
					
					break;
					
					case 3:out.print("无法完成操作,因为该订单已经处理完成");break;
					default:out.print("错误:无法更改当前订单");break;
						
					}
				}else 
				{
					out.print("出错，该订单不存在");
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				out.print("错误啦，传的参数不对啊");
				e.printStackTrace();
			}
			con.closeConnection();
		}
	}

}
