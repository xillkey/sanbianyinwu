package com.sanbianyinwu.www.Admin.DocItemOperation;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sanbianyinwu.www.Public.Function_Library;
import com.sanbianyinwu.www.Public.JDBConnection;

/**
 * Servlet implementation class HandleDocItem
 */

public class HandleDocItem extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HandleDocItem() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Cookie adminCookie = null;
		Cookie adminIDcCookie = null;
		String itemID = request.getParameter("itemID");
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter  out = response.getWriter();
		Cookie[] cookies = request.getCookies(); 
				adminIDcCookie = Function_Library.findCookie(cookies, "LoginID");
				adminCookie = Function_Library.findCookie(cookies, "adminRight");
				
				if(adminCookie==null||adminCookie.getValue().equals(""))
				{
					
					out.print("登陆超时");
				}else if(itemID!=null) {
					String adminID = adminIDcCookie.getValue();
					JDBConnection con = new JDBConnection();
					ResultSet temp = con.executeQuery("select adminID from yx_docitem where itemID="+itemID);
					try {
						if(temp.next())
						{
							if(temp.getInt(1)!=0) //订单已被处理
							{
								out.print("该订单已经在处理中");
							}else {
								if(con.executeUpdate("update yx_docitem set adminID = "+adminID +",itemStatus = 2 where itemID = "+itemID )!=-1)
								{
									out.print("你已接手处理该订单<br>请在“我接手的订单”中查看，并进行进一步操作");
								}
							}
								
						}
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					con.closeConnection();
				}else {
					out.print("出错啦");
				}
		
	}

}
