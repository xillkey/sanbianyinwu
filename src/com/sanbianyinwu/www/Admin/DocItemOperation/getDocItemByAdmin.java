package com.sanbianyinwu.www.Admin.DocItemOperation;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sanbianyinwu.www.Public.JDBConnection;
import com.sanbianyinwu.www.Public.DocItem;

public class getDocItemByAdmin extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		String adminName = "admin";//request.getParameter("adminName");
		int count = 0;
		JDBConnection con = new JDBConnection();
		String countsql = "select count(*) from yx_docitem join (select adminID , TrueName from yx_admin where TrueName ='" 
				+ adminName +"') admin on( yx_docitem.adminID = admin.adminID ) ";
		ResultSet countrs = con.executeQuery(countsql);
		try {
			countrs.next();
			count = countrs.getInt(1);
			countrs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		int page=Integer.parseInt(request.getParameter("page") == null ? "1" 
                : request.getParameter("page")); 
		Integer pagestart = (page - 1) * 20;
		String sql = "select itemType, TrueName,copies,subTime,money,itemStatus,docFile " +
				"from yx_docitem join " +
				"(select adminID , TrueName adminTrueName from yx_admin " +
				"where TrueName = '"+ adminName + "') admin " +
				"on( yx_docitem.adminID = admin.adminID )" +
				"order by subTime desc limit " + pagestart.toString() + ", 20";
		ResultSet rs = con.executeQuery(sql);
		List <DocItem> list = new ArrayList<DocItem>();
		try{
			while(rs.next()){
				DocItem DI = new DocItem();
				DI.setitemType(rs.getInt("itemType"));
				DI.setTrueName(rs.getString("TrueName"));
				DI.setcopies(rs.getInt("copies"));
				DI.setitemStatus(rs.getInt("itemStatus"));
				DI.setdocFile(rs.getString("docFile"));
				DI.setSubTime(rs.getLong("SubTime")); 
				DI.setmoney(rs.getFloat("money"));
				list.add(DI);
			}
			rs.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
		con.closeConnection();
		request.setAttribute("count",count);
		request.setAttribute("list", list);
		request.getRequestDispatcher("administrator/GetInformation.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}
	
}
