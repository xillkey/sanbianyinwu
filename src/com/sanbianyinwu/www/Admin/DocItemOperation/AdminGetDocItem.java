package com.sanbianyinwu.www.Admin.DocItemOperation;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;










import com.sanbianyinwu.www.Public.Function_Library;
import com.sanbianyinwu.www.Public.JDBConnection;
import com.sanbianyinwu.www.Public.DocItem;

public class AdminGetDocItem extends HttpServlet {
	private static final long serialVersionUID = 1L;


	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		Cookie IDCookie = null;
		Cookie adminRightCookie = null;
		Cookie[] cookies = request.getCookies();
		 // request.getParameter("userID");
		IDCookie=Function_Library.findCookie(cookies, "LoginID");
		adminRightCookie = Function_Library.findCookie(cookies, "adminRight");
		String ID = null;
		String adminRight = null;
			if (IDCookie != null) {
				ID = IDCookie.getValue();
			}	
			if (adminRightCookie != null) {
				adminRight = adminRightCookie.getValue();
				
			}
		 
		if(ID==null||ID.equals("")||adminRight==null||adminRight.equals(""))
		{
			System.out.print("error");
		}else
		{	
			int count = 0;
			JDBConnection con = new JDBConnection();
			String sql = "select count(*) from yx_docitem where adminID = " + ID ;
			ResultSet CountRS = con.executeQuery(sql);
			try {
				if(CountRS.next())
					 count = CountRS.getInt(1);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			int page=Integer.parseInt(request.getParameter("page")==null?"1":request.getParameter("page"));
			int pagestart = (page - 1)*5;
			//int adminID = Integer.parseInt(ID);
			sql = "select * from yx_docitem WHERE adminID = "+ ID + " order by subTime desc limit " + pagestart + ", 5";
			ResultSet rs = con.executeQuery(sql);
			List<DocItem> list = new LinkedList<DocItem>();
			try{
				while(rs.next()){
					DocItem DI = new DocItem();
					DI.setitemID(rs.getInt("itemID"));
					DI.setitemType(rs.getInt("itemType"));
					DI.setprintType(rs.getInt("printType"));
					DI.setuserID(rs.getInt("userID"));
					DI.setadminID(rs.getInt("adminID"));
					DI.setstudentNum(rs.getString("studentNum"));
					DI.setTrueName(rs.getString("TrueName"));
					DI.setpaperType(rs.getString("paperType"));
					DI.setcopies(rs.getInt("copies"));
					DI.setisColor(rs.getInt("isColor"));
					DI.setisSingle(rs.getInt("isSingle"));
					DI.setppts(rs.getInt("ppts"));
					DI.setotherRequest(rs.getString("otherRequest"));
					DI.setitemStatus(rs.getInt("itemStatus"));
					DI.setdocFile(rs.getString("docFile"));
					DI.setSubTime(rs.getLong("SubTime")); 
					DI.setTakeTime(rs.getLong("takeTime"));
					DI.setmoney(rs.getFloat("money"));
					list.add(DI);
				}
				rs.close();
			}catch(SQLException e){
				e.printStackTrace();
			}
		
		con.closeConnection();
		request.setAttribute("count",count);
		request.setAttribute("list", list);
		request.getRequestDispatcher("administrator/AdminGetDocItem.jsp").forward(request, response);
		}
	}


	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(req, resp);
	}

}
