package com.sanbianyinwu.www.Admin.DocItemOperation;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sanbianyinwu.www.Public.JDBConnection;
import com.sanbianyinwu.www.Public.DocItem;

/**
 * Servlet implementation class GetDocItemDetail
 */
//@WebServlet("/GetDocItemDetail")
public class GetDocItemDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetDocItemDetail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String itemID = request.getParameter("itemID");
		if(itemID != null)
		{
			String sql = "select * from yx_docitem where itemID="+itemID;
			
			JDBConnection con = new JDBConnection();
			ResultSet rs = con.executeQuery(sql);
			DocItem DI = new DocItem();
			try {
				if(rs.next())
				{
					DI.setitemID(rs.getInt("itemID"));
					DI.setitemType(rs.getInt("itemType"));
					DI.setprintType(rs.getInt("printType"));
					DI.setuserID(rs.getInt("userID"));
					DI.setadminID(rs.getInt("adminID"));
					DI.setstudentNum(rs.getString("studentNum"));
					DI.setTrueName(rs.getString("TrueName"));
					DI.setpaperType(rs.getString("paperType"));
					DI.setcopies(rs.getInt("copies"));
					DI.setisColor(rs.getInt("isColor"));
					DI.setisSingle(rs.getInt("isSingle"));
					DI.setppts(rs.getInt("ppts"));
					DI.setotherRequest(rs.getString("otherRequest"));
					DI.setitemStatus(rs.getInt("itemStatus"));
					DI.setdocFile(rs.getString("docFile"));
					DI.setSubTime(rs.getLong("subTime"));
					DI.setTakeTime(rs.getLong("takeTime"));
					DI.setmoney(rs.getFloat("money"));
					//System.out.println(DI.getcopies());
					//System.out.println(DI.getdocFile());
					rs.close();
					con.closeConnection();
				}else {
					//System.out.println("不存在此订单");
					DI=null;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			request.setAttribute("item", DI);
			request.getRequestDispatcher("WEB-INF/itemDetail.jsp").forward(request, response);
			
			
		}
	}

}
