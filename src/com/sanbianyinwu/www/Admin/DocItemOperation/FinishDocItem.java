package com.sanbianyinwu.www.Admin.DocItemOperation;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sanbianyinwu.www.Public.JDBConnection;

/**
 * Servlet implementation class FinishDocItem
 */
public class FinishDocItem extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FinishDocItem() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String money = request.getParameter("money");
		String itemID = request.getParameter("itemID");
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		if(money==null||itemID==null)
		{
			out.print("出错啦");
		}else{
			JDBConnection con = new JDBConnection();
			if(con.executeUpdate("update yx_docitem set money = "+money+" ,itemStatus = 3 where itemID = "+itemID+" and itemStatus=2")>0)
			{
				out.print("已成功把订单状态修改成  处理完成");
				
			}else{
				out.print("你的输入有误");
			}
			con.closeConnection();
			
		}
		
	}

}
