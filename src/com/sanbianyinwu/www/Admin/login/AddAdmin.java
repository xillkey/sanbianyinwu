package com.sanbianyinwu.www.Admin.login;

import java.io.IOException;
import java.net.URLDecoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sanbianyinwu.www.Public.Function_Library;

/**
 * Servlet implementation class AddAdmin
 */
//@WebServlet("/AddAdmin")
public class AddAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddAdmin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Cookie[] cookies = request.getCookies();
		Cookie adminRightCookie = Function_Library.findCookie(cookies, "adminRight");
		if(adminRightCookie!=null && "超级管理员".equals(URLDecoder.decode(adminRightCookie.getValue(), "UTF-8"))) //URLDecoder.decode(adminRightCookie.getValue(), "UTF-8")
		{
			request.getRequestDispatcher("administrator/register/adminRegister.jsp").forward(request, response);
		}else {
			response.sendRedirect("index.jsp");
		}
			
	}

}
